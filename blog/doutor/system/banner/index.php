<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>
    </div>
    <div class="page-title">
        <div class="title col-md-12 col-sm-6 col-xs-12">
            <h3><i class="fa fa-desktop"></i> Lista de banner cadastrados</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    if (isset($get) && $get == true && isset($_SESSION['Error'])):
                    //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                    unset($_SESSION['Error']);
                endif;
                ?>
                <div class="x_content">
                    <div class="x_title">
                        <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode, acessar, editar, remover e alterar os status dos banners</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped dtr-inline dataTable jambo_table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Imagem</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $ReadRecursos = new Read;
                                $ReadRecursos->ExeRead(TB_BANNER);
                                if (!$ReadRecursos->getResult()):
                                    WSErro("Nenhum banner foi encontrado.", WS_INFOR, null, "Aviso!");
                                else:
                                    foreach ($ReadRecursos->getResult() as $dados):
                                        extract($dados);
                                        ?>
                                        <tr class="j_item" id="<?= $sl_id; ?>">
                                            <th scope="row"><?= $sl_id; ?></th>
                                            <th><a href="<?= BASE . "/uploads/" . $sl_file; ?>" target="_blank" title="<?= $sl_title; ?>" class="btn border-dark j_modalBox" style="width: 100px; height: 100px; overflow: hidden;"><?= Check::Image("../doutor/uploads/" . $sl_file, $sl_title, 'img-responsive', 100, 100); ?></a></th>
                                            <td><?= $sl_title; ?></td>
                                            <td><?= Check::Words($sl_description, 25); ?></td>
                                            <td class="j_GetStatusRecursos" rel="<?= $sl_id; ?>"></td>
                                            <td style="width: 110px;">
                                                <div class="btn-group btn-group-xs">
                                                    <a class="btn btn-dark" href="../" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=banner/update&id=<?= $sl_id; ?>'"><i class="fa fa-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger j_remove" rel="<?= $sl_id; ?>" action="RemoveBanner"><i class="fa fa-trash"></i></button>
                                                    <button type="button" class="btn j_statusRecursos btn-default" rel="<?= $sl_id; ?>" tabindex="getStatusBanner" action="StatusBanner" value="<?= $sl_status; ?>"><i class="fa fa-ban"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>