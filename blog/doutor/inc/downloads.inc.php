<hr>
<ul class="list-unstyled text-left" id="callbacks">
  <?php
  // View dos downloads cadastrados para uso de relacionamento de urls
  $ReadDow = new Read;
  $ReadDow->ExeRead(TB_DOWNLOAD, "WHERE dow_status = :st AND user_empresa = :emp ORDER BY dow_date DESC", "st=2&emp={$_SESSION['userlogin']['user_empresa']}");
  if ($ReadDow->getResult()):
    foreach ($ReadDow->getResult() as $dow):
      ?>
      <li class="j_dowloads" data-filter='<?= strtolower($dow["dow_title"]); ?>'>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
          <div class="checkbox">
            <label for="<?= $dow['dow_title']; ?>">
              <input type="checkbox" class="flat" id="<?= $dow['dow_title']; ?>" name="url_relation[]" value="<?= $dow['dow_id']; ?>"
              <?php
              $url_re = explode(',', $url_relation);
              foreach ($url_re as $urlsr):
                if (isset($post['url_relation']) && $post['url_relation'] == $dow['dow_id']):
                  echo 'checked="true"';
                elseif ($urlsr == $dow['dow_id']):
                  echo 'checked="true"';
                endif;
              endforeach;
              ?>> <?= $dow['dow_title'] . ' | <span class="label label-primary">Data: ' . date("d/m/Y H:i:s", strtotime($dow['dow_date'])) . "</span>"; ?>
            </label>
          </div>
        </div>
      </li>
      <?php
    endforeach;
  endif;
  ?>
</ul>
<div class="clearfix"></div>
<br>