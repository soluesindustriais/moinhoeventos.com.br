<?php
session_start();
$getPost = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$setPost = array_map('strip_tags', $getPost);
$post = array_map('trim', $setPost);
$Action = $post['action'];
$jSon = null;
unset($post['action']);
if ($Action):
  require('../../_app/Config.inc.php');
  $Read = new Read;
  $Create = new Create;
  $Update = new Update;
  $Delete = new Delete;
endif;
usleep(50000);
switch ($Action):
  case 'setIdioma':
  $id = (int) $post['itemId'];
  $idemp = (int) $_SESSION['userlogin']['user_empresa'];
  $Read->ExeRead(TB_EMP, "WHERE empresa_id = :sess", "sess={$idemp}");
  if (!$Read->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$id}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $readNow = $Read->getResult();
      $_SESSION['userlogin']['user_empresa'] = $readNow[0]['empresa_id'];
      $jSon['callback'] = $readNow[0]['empresa_idioma_name'] . ' ' . Check::GetIdioma($readNow[0]['empresa_idioma']) . ' <i class="fa fa-angle-down"></i>';
    endif;
  endif;
  break;
  case 'RemoveIdioma':
  $id = (int) $post['itemId'];
  $idemp = (int) $_SESSION['userlogin']['user_empresa'];
  $level = (int) 3;
  $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id AND (empresa_id = :sess OR empresa_parent = :sess)", "id={$id}&sess={$idemp}");
  if (!$Read->getResult()):
    $jSon['error'] = true;
  elseif ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['result'] = 0;
  else:
    $read = $Read->getResult();
    $arr = array('empresa_idioma_status' => 3);
    $Update->ExeUpdate(TB_EMP, $arr, "WHERE empresa_id = :id AND (empresa_id = :sess OR empresa_parent = :sess)", "id={$id}&sess={$_SESSION['userlogin']['user_empresa']}");
    if (!$Update->getResult()):
      $jSon['error'] = true;
    else:
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Idiomas", "Removeu o idioma: {$read[0]['empresa_idioma_name']}", date("Y-m-d H:i:s"));
      $jSon = true;
    endif;
  endif;
  break;
  case 'StatusIdioma':
  $id = (int) $post['itemId'];
  $st = (int) $post['ativo'];
  $idemp = (int) $_SESSION['userlogin']['user_empresa'];
  $level = (int) 3;
  if ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id AND (empresa_id = :sess OR empresa_parent = :sess)", "id={$id}&sess={$idemp}");
    if (!$Read->getResult()):
      $jSon['result'] = 0;
    else:
      $upd = array('empresa_idioma_status' => $st);
      $Update->ExeUpdate(TB_EMP, $upd, "WHERE empresa_id = :id", "id={$id}");
      if ($Update->getResult() != 0):
        $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id AND (empresa_id = :sess OR empresa_parent = :sess)", "id={$id}&sess={$idemp}");
        $read = $Read->getResult();
        if (!$Read->getResult()):
          $jSon['result'] = 0;
        else:
          $jSon = array('id' => $read[0]['empresa_id'], 'status' => $read[0]['empresa_idioma_status']);
          Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Idiomas", "Atualizou o status do idioma {$read[0]['empresa_idioma_name']}", date("Y-m-d H:i:s"));
        endif;
      endif;
    endif;
  endif;
  break;
  case "GravaImport":
  $level = 4;
  $jSon['callback'] = null;
  if ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['error'] = 0;
  else:
    $cadastra = new GravaImport();
    $cadastra->saveData($post['dados'], $post['configs'], $post['linha']);
    if ($cadastra->getResult()):
      $jSon['registro'] = (int) $post['linha'];
      $jSon['callbacks'] = $cadastra->getCallback();
    else:
      $jSon['registro'] = (int) $post['linha'];
      $jSon['callbacks'] = $cadastra->getCallback();
    endif;
  endif;
  break;
  case "ImportCSV":
// RECEBE O ARQUIVO
  $post['file'] = ( $_FILES['csv_file']['tmp_name'] ? $_FILES['csv_file'] : null );
  $post['file']['table'] = $post['tabela'];
  $post['file']['baseDir'] = $post['baseDir'];
  $post['file']['cat_parent'] = $post['cat_parent'];
  $level = 3;
  $jSon['callback'] = null;
  if ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['error'] = 0;
  else:
    $import = new ImportFiles($post['file']);
    if (!$import->getResult()):
      $jSon['error'] = $import->getError();
    else:
      $jSon['callback'] .= "
      <div class='x_title'>
      <h2>Foram encontrados " . count($import->getData()) . " registro(s)</h2>
      <div class='clearfix'></div>
      </div>

      <div class='alert alert-info alert-dismissible fade in' role='alert'>
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
      O processo de <strong>importação</strong> pode levar alguns minutos, você pode acompanhar abaixo o relatório de operação.
      </div>
      <div class='clearfix'></div>

      <div class='j_progress'>
      <div class='text-info'>Cadastrando...</div>
      <div class='progress'>
      <div class='progress-bar progress-bar-primary' data-transitiongoal='0'></div>
      </div>
      </div>

      ";
      $jSon['total'] = count($import->getData());
      $jSon['result'] = $import->getError();
      $jSon['import'] = $import->getData();
      $jSon['allDados'] = $import->getConfigs();
    endif;
  endif;
  break;
// CADASTRO DE DOWNLOADS VIA AJAX
  case "CadastraDown":
  $level = 2;
  if ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['error'] = 0;
  else:
    $post['dow_status'] = 2;
    $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
    $post['dow_file'] = ( $_FILES['dow_file']['tmp_name'] ? $_FILES['dow_file'] : null );
    $cadastra = new Downloads();
    $cadastra->ExeCreate($post);
    if (!$cadastra->getResult()):
      $jSon['error'] = $cadastra->getError();
    else:
      $Read->ExeRead(TB_DOWNLOAD, "WHERE user_empresa = :emp AND dow_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$cadastra->getLastId()}");
      if (!$Read->getResult()):
        $jSon['error'] = 1;
      else:
        foreach ($Read->getResult() as $dow):
          $jSon['callback'] = "<li class='j_dowloads' data-filter='" . strtolower($dow['dow_title']) . "'>
          <div class='col-md-12 col-sm-12 col-xs-12 form-group'>
          <div class='checkbox'>
          <label for='" . $dow['dow_title'] . "' class=''>
          <div class='icheckbox_flat-green checked' style='position: relative;'>
          <input type='checkbox' class='flat' id='" . $dow['dow_title'] . "' data-parsley-multiple='url_relation' style='position: absolute; opacity: 0;' name='url_relation[]' value='" . $dow['dow_id'] . "' checked='true'>
          <ins class='iCheck-helper' style='position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;'></ins>
          </div> " . $dow['dow_title'] . " | <span class='label label-primary'>Data: " . date('d/m/Y H:i:s', strtotime($dow['dow_date'])) . "</span> <span class='label label-success'>Novo</span>
          </label>
          </div>
          </div>
          </li>";
        endforeach;
      endif;
      $jSon['result'] = $cadastra->getError();
    endif;
  endif;
  break;
  // NEWSLETTER
  case "GravaNewsletter":
  $cadastra = new Newsletter;
  $cadastra->Cadastrar($post);
  if (!$cadastra->getResult()):
    $jSon['error'] = $cadastra->getError();
  else:
    $jSon['result'] = $cadastra->getError();
  endif;
  break;
  // PAGE IMAGE
  case 'sendimage':
  $post['user_empresa'] = (int) $_SESSION['userlogin']['user_empresa'];
  $level = (int) 2;
  $NewImage = ($_FILES['gallery_file']['tmp_name'] ? $_FILES['gallery_file'] : null);
  if ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['error'] = 0;
  else:
    $ImgName = "post-{$NewImage['tmp_name']}-" . (substr(md5(time().$NewImage['tmp_name']), 0, 20));
    $Upload = new Upload('../../uploads/');
    $Upload->Image($NewImage, $ImgName, 800, $post['user_empresa'], 'posts');
    if (!$Upload->getResult()):
      $jSon['error'] = 1;
      $jSon['mensagem'] = $Upload->getError();
    else:
      $post['gallery_file'] = $Upload->getResult();
      $Create->ExeCreate(TB_GALLERY, $post);
      if (!$Create->getResult()):
        $jSon['error'] = 2;
      else:
        $Read->ExeRead(TB_GALLERY, "WHERE gallery_id = :id AND user_empresa = :emp", "id={$Create->getResult()}&emp={$post['user_empresa']}");
        if (!$Read->getResult()):
          $jSon['error'] = 3;
        else:
          $read = $Read->getResult();
          $jSon['tinyMCE'] = "<img title='Imagem de ".SITE_NAME."' alt='Imagem de ".SITE_NAME."' src='" . BASE . "/uploads/{$read[0]['gallery_file']}'/>";
        endif;
      endif;
    endif;
  endif;
  break;
  case 'removeProdutoCart':
  $id = $post['prod_id'];
  if (isset($_SESSION['CARRINHO'])):
    unset($_SESSION['CARRINHO'][$id]);
    $jSon['result'] = 0;
    $jSon['total'] = 0.00;
    foreach ($_SESSION['CARRINHO'] as $keys):
      foreach ($keys['modelos'] as $cont):
        $jSon['result'] += $cont;
        $jSon['result_id'] = $id;
        $contas = count($_SESSION['CARRINHO']);
        if ($contas == 0):
          unset($_SESSION['CARRINHO']);
        endif;
        $jSon['total'] += ($keys['prod_preco'] * $cont);
      endforeach;
    endforeach;
    $jSon['total'] = number_format($jSon['total'], 2, ',', '.');
  endif;
  break;
  case 'removeItemCart':
  if (empty($_SESSION['CARRINHO'][$post['prod_id']]['modelos'])):
    $jSon['error'] = true;
  else:
    unset($_SESSION['CARRINHO'][$post['prod_id']]['modelos'][$post['prod_item']]);
    if (empty($_SESSION['CARRINHO'][$post['prod_id']]['modelos'])):
      unset($_SESSION['CARRINHO'][$post['prod_id']]);
      $jSon['model'] = 0;
    endif;
    $jSon['result_id'] = $post['prod_id'];
    $jSon['result_item'] = $post['prod_item'];
    $jSon['result'] = 0;
    $jSon['total'] = 0.00;
    foreach ($_SESSION['CARRINHO'] as $keys):
      foreach ($keys['modelos'] as $cont):
        $jSon['result'] += $cont;
        $jSon['total'] += ($keys['prod_preco'] * $cont);
      endforeach;
    endforeach;
    $jSon['total'] = number_format($jSon['total'], 2, ',', '.');
  endif;
  break;
  case 'updateCart':
  unset($post['base']);
  $Read->ExeRead(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp AND prod_status = 2", "id={$post['prod_id']}&emp=" . EMPRESA_CLIENTE);
  if (!$Read->getResult()):
    $jSon['error'] = true;
  else:
    $PROD = $Read->getResult();
    $_SESSION['CARRINHO'][$post['prod_id']]['modelos'][$post['prod_item']] = (int) $post['prod_qtd'];
    $jSon['result'] = 0;
    $jSon['total'] = 0.00;
    foreach ($_SESSION['CARRINHO'] as $keys):
      foreach ($keys['modelos'] as $cont):
        $jSon['result'] += $cont;
        $jSon['total'] += ($keys['prod_preco'] * $cont);
      endforeach;
    endforeach;
    $jSon['total'] = number_format($jSon['total'], 2, ',', '.');
    $jSon['produto'] = "<b>{$post['prod_qtd']}</b> iten(s) {$PROD[0]['prod_title']} <b>Modelo:</b> {$post['prod_item']} adicionado(s) no orçamento.";
  endif;
  break;
  case 'addCart':
  unset($post['base']);
  $Read->ExeRead(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp AND prod_status = 2", "id={$post['prod_id']}&emp=" . EMPRESA_CLIENTE);
  if (!$Read->getResult()):
    $jSon['error'] = true;
  else:
    $PROD = $Read->getResult();
    if (!isset($_SESSION['CARRINHO'])):
      $_SESSION['CARRINHO'] = array();
    endif;
    if (!isset($_SESSION['CARRINHO'][$post['prod_id']])):
      $_SESSION['CARRINHO'][$post['prod_id']] = array(
        'prod_codigo' => $PROD[0]['prod_codigo'],
        'prod_title' => $PROD[0]['prod_title'],
        'prod_preco' => ( isset($PROD[0]['prod_preco']) && !empty($PROD[0]['prod_preco']) ? $PROD[0]['prod_preco'] : 'Indisponível' ),
        'modelos' => array($post['prod_item'] => (int) $post['prod_qtd'])
      );
    else:
      $_SESSION['CARRINHO'][$post['prod_id']]['modelos'][$post['prod_item']] += (int) $post['prod_qtd'];
    endif;
    $jSon['result'] = 0;
    foreach ($_SESSION['CARRINHO'] as $keys):
      foreach ($keys['modelos'] as $cont):
        $jSon['result'] += $cont;
      endforeach;
    endforeach;
    $jSon['produto'] = "<b>{$post['prod_qtd']}</b> iten(s) {$PROD[0]['prod_title']} <b>Modelo:</b> {$post['prod_item']} adicionado(s) no orçamento.";
  endif;
  break;
  case 'cartView':
  if (!isset($_SESSION['CARRINHO'])):
    $jSon['result'] = 0;
    $jSon['total'] = 0.00;
  else:
    $jSon['result'] = 0;
    $jSon['total'] = 0.00;
    foreach ($_SESSION['CARRINHO'] as $keys):
      foreach ($keys['modelos'] as $cont):
        $jSon['result'] += $cont;
        $jSon['total'] += ($keys['prod_preco'] * $cont);
      endforeach;
    endforeach;
    $jSon['total'] = number_format($jSon['total'], 2, ',', '.');
  endif;
  break;
  // REMOVE IMAGEM SINGLE DAS GALERIA
  case 'RemoveImagem':
  $id = (int) $post['itemId'];
  $idemp = (int) $_SESSION['userlogin']['user_id'];
  $level = (int) 2;
  $Read->ExeRead(TB_GALLERY, "WHERE gallery_id = :id", "id={$id}");
  if (!$Read->getResult()):
    $jSon['error'] = true;
  elseif ($_SESSION['userlogin']['user_level'] < $level):
    $jSon['result'] = 0;
  else:
    $read = $Read->getResult();
    $Delete->ExeDelete(TB_GALLERY, "WHERE gallery_id = :id", "id={$id}");
    if (!$Delete->getResult()):
      $jSon['error'] = true;
    else:
      $delimagem = $read[0]['gallery_file'];
      if (file_exists("../../uploads/{$delimagem}") && !is_dir("../../uploads/{$delimagem}")):
        unlink("../../uploads/{$delimagem}");
    endif;
    Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Removeu", "Imagem da galeria: " . Check::GetEmpresaName($read[0]['user_empresa']), date("Y-m-d H:i:s"));
    $jSon = true;
  endif;
endif;
break;
  // QUEM SOMOS
case 'getStatusQuem':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_QUEMSOMOS, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['quem_id'], 'ret_status' => $key['quem_status']);
  endforeach;
endif;
break;
case 'StatusQuem':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_QUEMSOMOS, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('quem_status' => $status);
  $Update->ExeUpdate(TB_QUEMSOMOS, $set, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_QUEMSOMOS, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['quem_id'], 'ret_status' => $read[0]['quem_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notícias", "Atualizou o status da noticia {$read[0]['quem_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveQuem':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_QUEMSOMOS, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_parent'])):
    $Read->ExeRead(TB_QUEMSOMOS, "WHERE cat_parent = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_QUEMSOMOS, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $delCapa = $read[0]['quem_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Quem somos", "Removeu a página: {$read[0]['quem_title']}", date("Y-m-d H:i:s"));
      $jSon = true;
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_QUEMSOMOS, "WHERE quem_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['quem_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Quem somos", "Removeu a página: {$read[0]['quem_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
endif;
break;
  // VAGAS
case 'getStatusVaga':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['vaga_id'], 'ret_status' => $key['vaga_status']);
  endforeach;
endif;
break;
case 'StatusVaga':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('vaga_status' => $status);
  $Update->ExeUpdate(TB_VAGA, $set, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['vaga_id'], 'ret_status' => $read[0]['vaga_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Vagas", "Atualizou o status da vaga {$read[0]['vaga_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveVaga':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['vaga_parent'])):
    $Read->ExeRead(TB_VAGA, "WHERE vaga_parent = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $jSon = true;
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Vagas", "Removeu a área: {$read[0]['vaga_title']}", date("Y-m-d H:i:s"));
      endif;
    endif;
  else:
    $Delete->ExeDelete(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Delete->getResult()):
      $jSon['error'] = true;
    else:
      $jSon = true;
      $read = $Read->getResult();
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Vagas", "Removeu a vaga: {$read[0]['vaga_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
  // DOWNLOADS
case 'getStatusDow':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['dow_id'], 'ret_status' => $key['dow_status']);
  endforeach;
endif;
break;
case 'StatusDow':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('dow_status' => $status);
  $Update->ExeUpdate(TB_DOWNLOAD, $set, "WHERE dow_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['dow_id'], 'ret_status' => $read[0]['dow_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Downloads", "Atualizou o status do download {$read[0]['dow_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveDow':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['dow_file'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Downloads", "Removeu o download: {$read[0]['dow_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
break;
  // CASES
case 'getStatusCase':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_CASE, "WHERE case_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['case_id'], 'ret_status' => $key['case_status']);
  endforeach;
endif;
break;
case 'StatusCase':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_CASE, "WHERE case_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('case_status' => $status);
  $Update->ExeUpdate(TB_CASE, $set, "WHERE case_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_CASE, "WHERE case_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['case_id'], 'ret_status' => $read[0]['case_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Cases", "Atualizou o status do case {$read[0]['case_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveCase':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_CASE, "WHERE case_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_CASE, "WHERE case_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['case_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Cases", "Removeu o case: {$read[0]['case_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
break;
  // BANNER
case 'getStatusBanner':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_BANNER, "WHERE sl_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['sl_id'], 'ret_status' => $key['sl_status']);
  endforeach;
endif;
break;
case 'StatusBanner':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_BANNER, "WHERE sl_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('sl_status' => $status);
  $Update->ExeUpdate(TB_BANNER, $set, "WHERE sl_id = :id", "id={$id}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_BANNER, "WHERE sl_id = :id", "id={$id}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['sl_id'], 'ret_status' => $read[0]['sl_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Banner", "Atualizou o status do banner {$read[0]['sl_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveBanner':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_BANNER, "WHERE sl_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_BANNER, "WHERE sl_id = :id", "id={$id}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['sl_file'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Banner", "Removeu o banner: {$read[0]['sl_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
break;
  // CLIENTES
case 'getStatusCli':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['cli_id'], 'ret_status' => $key['cli_status']);
  endforeach;
endif;
break;
case 'StatusCli':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('cli_status' => $status);
  $Update->ExeUpdate(TB_CLIENTE, $set, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['cli_id'], 'ret_status' => $read[0]['cli_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Atualizou o status da postagem {$read[0]['cli_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveCliente':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_parent'])):
    $Read->ExeRead(TB_CLIENTE, "WHERE cat_parent = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $delCapa = $read[0]['cli_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Clientes", "Removeu o cliente: {$read[0]['cli_title']}", date("Y-m-d H:i:s"));
      $jSon = true;
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['cli_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Clientes", "Removeu o cliente: {$read[0]['cli_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
endif;
break;
  // EMPRESAS
case 'getStatusEmpresa':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_EMPRESA, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['emp_id'], 'ret_status' => $key['emp_status']);
  endforeach;
endif;
break;
case 'StatusEmpresa':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_EMPRESA, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('emp_status' => $status);
  $Update->ExeUpdate(TB_EMPRESA, $set, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_EMPRESA, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['emp_id'], 'ret_status' => $read[0]['emp_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Atualizou o status da postagem {$read[0]['emp_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveEmpresa':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_EMPRESA, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_parent'])):
    $Read->ExeRead(TB_EMPRESA, "WHERE cat_parent = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_EMPRESA, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $delCapa = $read[0]['emp_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresa", "Removeu a empresa: {$read[0]['emp_title']}", date("Y-m-d H:i:s"));
      $jSon = true;
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_EMPRESA, "WHERE emp_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['emp_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresas", "Removeu a empresa: {$read[0]['emp_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
endif;
break;
  // SERVIÇOS
case 'getStatusServ':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_SERVICO, "WHERE serv_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['serv_id'], 'ret_status' => $key['serv_status']);
  endforeach;
endif;
break;
case 'StatusServ':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_SERVICO, "WHERE serv_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('serv_status' => $status);
  $Update->ExeUpdate(TB_SERVICO, $set, "WHERE serv_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_SERVICO, "WHERE serv_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['serv_id'], 'ret_status' => $read[0]['serv_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Produtos", "Atualizou o status do produto {$read[0]['serv_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveServico':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_SERVICO, "WHERE serv_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_SERVICO, "WHERE serv_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['serv_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  $Read->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if ($Read->getResult()):
    foreach ($Read->getResult() as $deleta):
      if (file_exists("../../uploads/{$deleta['gallery_file']}") && !is_dir("../../uploads/{$deleta['gallery_file']}")):
        unlink("../../uploads/{$deleta['gallery_file']}");
    endif;
  endforeach;
endif;
Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Produtos", "Removeu o produto: {$read[0]['serv_title']}", date("Y-m-d H:i:s"));
$jSon = true;
endif;
endif;
break;
  // PRODUTOS
case 'getStatusProd':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['prod_id'], 'ret_status' => $key['prod_status']);
  endforeach;
endif;
break;
case 'StatusProd':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('prod_status' => $status);
  $Update->ExeUpdate(TB_PRODUTO, $set, "WHERE prod_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['prod_id'], 'ret_status' => $read[0]['prod_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Produtos", "Atualizou o status do produto {$read[0]['prod_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveProduto':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_PRODUTO, "WHERE prod_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['prod_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  $Read->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if ($Read->getResult()):
    foreach ($Read->getResult() as $deleta):
      if (file_exists("../../uploads/{$deleta['gallery_file']}") && !is_dir("../../uploads/{$deleta['gallery_file']}")):
        unlink("../../uploads/{$deleta['gallery_file']}");
    endif;
  endforeach;
endif;
Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Produtos", "Removeu o produto: {$read[0]['prod_title']}", date("Y-m-d H:i:s"));
$jSon = true;
endif;
endif;
break;
  // PÁGINAS
case 'getStatusPag':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_PAGINA, "WHERE pag_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['pag_id'], 'ret_status' => $key['pag_status']);
  endforeach;
endif;
break;
case 'StatusPag':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_PAGINA, "WHERE pag_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('pag_status' => $status);
  $Update->ExeUpdate(TB_PAGINA, $set, "WHERE pag_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_PAGINA, "WHERE pag_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['pag_id'], 'ret_status' => $read[0]['pag_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Páginas", "Atualizou o status da página {$read[0]['pag_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemovePagina':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_PAGINA, "WHERE pag_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_PAGINA, "WHERE pag_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Páginas", "Removeu a página: {$read[0]['pag_title']}", date("Y-m-d H:i:s"));
    $jSon = true;
  endif;
endif;
break;
  // BLOG
case 'getStatusBlog':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_BLOG, "WHERE blog_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['blog_id'], 'ret_status' => $key['blog_status']);
  endforeach;
endif;
break;
case 'StatusBlog':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_BLOG, "WHERE blog_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('blog_status' => $status);
  $Update->ExeUpdate(TB_BLOG, $set, "WHERE blog_id = :id", "id={$id}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_BLOG, "WHERE blog_id = :id", "id={$id}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['blog_id'], 'ret_status' => $read[0]['blog_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Atualizou o status da postagem {$read[0]['blog_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveBlog':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_BLOG, "WHERE blog_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_parent'])):
    $Read->ExeRead(TB_BLOG, "WHERE cat_parent = :id", "id={$id}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_BLOG, "WHERE blog_id = :id", "id={$id}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $delCapa = $read[0]['blog_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Removeu a postagem: {$read[0]['blog_title']}", date("Y-m-d H:i:s"));
      $jSon = true;
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_BLOG, "WHERE blog_id = :id", "id={$id}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['blog_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Removeu a postagem: {$read[0]['blog_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
endif;
break;
  // NOTICIAS
case 'getStatusNoti':
$id = (int) $post['_id'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_NOTICIA, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['noti_id'], 'ret_status' => $key['noti_status']);
  endforeach;
endif;
break;
case 'StatusNoticia':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_NOTICIA, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('noti_status' => $status);
  $Update->ExeUpdate(TB_NOTICIA, $set, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_NOTICIA, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['noti_id'], 'ret_status' => $read[0]['noti_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notícias", "Atualizou o status da noticia {$read[0]['noti_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveNoticia':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_NOTICIA, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_parent'])):
    $Read->ExeRead(TB_NOTICIA, "WHERE cat_parent = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_NOTICIA, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $delCapa = $read[0]['noti_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notícias", "Removeu a notícia: {$read[0]['noti_title']}", date("Y-m-d H:i:s"));
      $jSon = true;
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_NOTICIA, "WHERE noti_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['noti_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notícias", "Removeu a notícia: {$read[0]['noti_title']}", date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
endif;
break;
  // CATEGORIA
case 'getStatusCateg':
$id = (int) $post['_id'];
// $idemp = (int) $_SESSION['userlogin']['user_empresa'];
$Read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('ret_id' => $key['cat_id'], 'ret_status' => $key['cat_status']);
  endforeach;
endif;
break;
case 'StatusCategoria':
$id = (int) $post['_id'];
$status = (int) $post['_status'];
// $idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$Read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $set = array('cat_status' => $status);
  $Update->ExeUpdate(TB_CATEGORIA, $set, "WHERE cat_id = :id", "id={$id}");
  if (!$Update->getResult()):
    $jSon['error'] = true;
  else:
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      $jSon = array('ret_id' => $read[0]['cat_id'], 'ret_status' => $read[0]['cat_status']);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Categorias", "Atualizou o status da categoria {$read[0]['cat_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'RemoveCapaCategoria':
$id = (int) $post['itemId'];
// $idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 2;
$updt['cat_cover'] = null;
$Read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_cover'])):
    $jSon['result'] = 2;
  else:
    $Update->ExeUpdate(TB_CATEGORIA, $updt, "WHERE cat_id = :id", "id={$id}");
    if ($Update->getResult() != 0):
      $delCapa = $read[0]['cat_cover'];
      if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
        unlink("../../uploads/{$delCapa}");
    endif;
    $jSon = true;
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Categorias", "Removeu a capa de: {$read[0]['cat_title']}", date("Y-m-d H:i:s"));
endif;
endif;
break;
case 'RemoveCategoria':
$id = (int) $post['itemId'];
// $idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  if (empty($read[0]['cat_parent'])):
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_parent = :id", "id={$id}");
    if ($Read->getResult()):
      $jSon['result'] = 3;
    else:
      $Delete->ExeDelete(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $jSon = true;
        $read = $Read->getResult();
        $delCapa = $read[0]['cat_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Categorias", "Removeu a sessão: {$read[0]['cat_title']}", date("Y-m-d H:i:s"));
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_CATEGORIA, "WHERE cat_id = :id", "id={$id}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $jSon = true;
    $read = $Read->getResult();
    Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Categorias", "Removeu a categoria: {$read[0]['cat_title']}", date("Y-m-d H:i:s"));
  endif;
endif;
endif;
break;
  // ORÇAMENTOS
case 'RemoveOrcamento':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_ORCAMENTOS, "WHERE id_orc = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_ORCAMENTOS, "WHERE id_orc = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['orc_anexo'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Orçamentos", "Removeu o orçamento de: {$read[0]['orc_nome']} recebido em:" . date('d/m/Y H:i:s', strtotime($read[0]['orc_data'])), date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
break;
  // CANDIDATO
case 'RemoveCandidato':
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 3;
$Read->ExeRead(TB_CANDIDATO, "WHERE cand_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['result'] = 0;
else:
  $read = $Read->getResult();
  $Delete->ExeDelete(TB_CANDIDATO, "WHERE cand_id = :id AND user_empresa = :emp", "id={$id}&emp={$idemp}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $delCapa = $read[0]['cand_file'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Candidatos", "Removeu o candidato: {$read[0]['cand_nome']} para a vaga de: " . Check::VagaById($read[0]['cand_vaga']) . ' Enviada em: ' . date('d/m/Y H:i:s', strtotime($read[0]['cand_date'])), date("Y-m-d H:i:s"));
  $jSon = true;
endif;
endif;
break;
  // REMOVE EMPRESA, PORÉM SÓ MUDA O STATUS DELA PARA NÃO APARECER MAIS NA LISTAGEM
case "RemoveEmpresaCliente":
$id = (int) $post['itemId'];
$idemp = (int) $_SESSION['userlogin']['user_empresa'];
$level = (int) 5;
if ($_SESSION['userlogin']['user_level'] < $level):
  $jSon['error'] = 2;
else:
  // VERIFICAR SE O CLIENTE JÁ EXISTE NO BANCO NOVO
  $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$id}");
  if (!$Read->getResult()):
    $jSon['error'] = 0;
  else:
    $Dadosemp = array("empresa_status" => 3);
    $Update->ExeUpdate(TB_EMP, $Dadosemp, "WHERE empresa_id = :id", "id={$id}");
    if (!$Update->getResult()):
      $jSon['error'] = 0;
    else:
      $jSon = true;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresa Clientes", "Removeu a empresa: {$cadastra[0]['empresa']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'verstatus':
$id = (int) $post['empresa_id'];
$Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $jSon = array('id' => $key['empresa_id'], 'status' => $key['empresa_status']);
  endforeach;
endif;
break;
case 'UpStatus':
if ($_SESSION['userlogin']['user_level'] > 5):
  $jSon['error'] = true;
else:
  $id = (int) $post['empresa_id'];
  unset($post['empresa_id']);
  $Update->ExeUpdate(TB_EMP, $post, "WHERE empresa_id = :id", "id={$id}");
  if ($Update->getResult() != 0):
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$id}");
    if (!$Read->getResult()):
      $jSon['error'] = true;
    else:
      foreach ($Read->getResult() as $key):
        $jSon = array('id' => $key['empresa_id'], 'status' => $key['empresa_status']);
      endforeach;
      $read = $Read->getResult();
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresa", "Atualizou o status da empresa {$read[0]['empresa_razaosocial']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
case 'setEmpresa':
$id = (int) $post['empresa_id'];
$Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
else:
  foreach ($Read->getResult() as $key):
    $_SESSION['userlogin']['user_empresa_old'] = $_SESSION['userlogin']['user_empresa'];
    $_SESSION['userlogin']['user_empresa'] = $key['empresa_id'];
    $jSon = true;
  endforeach;
endif;
break;
case 'removeUser':
$id = (int) $post['user_id'];
$idemp = (int) $post['user_emp'];
$level = (int) 3;
$Read->ExeRead(TB_USERS, "WHERE user_empresa = :id AND user_id = :user", "id={$idemp}&user={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($id == $_SESSION['userlogin']['user_id']):
  $jSon["result"] = 0;
elseif ($_SESSION['userlogin']['user_level'] > 3):
  $read = $Read->getResult();
  if ($read[0]['user_level'] == 3):
    $Read->ExeRead(TB_USERS, "WHERE user_id != :id AND user_level = :lv AND user_empresa = :emp", "id={$id}&lv={$level}&emp={$idemp}");
    if (!$Read->getRowCount()):
      $jSon["result"] = 1;
    else:
      $Delete->ExeDelete(TB_USERS, "WHERE user_id = :id", "id={$id}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $jSon = true;
        $read = $Read->getResult();
        $delCapa = $read[0]['user_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Removeu o usuário {$read[0]['user_name']} {$read[0]['user_lastname']}", date("Y-m-d H:i:s"));
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_USERS, "WHERE user_id = :id", "id={$id}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $jSon = true;
    $read = $Read->getResult();
    $delCapa = $read[0]['user_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Removeu o usuário {$read[0]['user_name']} {$read[0]['user_lastname']}", date("Y-m-d H:i:s"));
endif;
endif;
else:
  $jSon['result'] = 2;
endif;
break;
case 'removeFunc':
$id = (int) $post['user_id'];
$idemp = (int) $post['user_emp'];
$level = (int) 5;
$Read->ExeRead(TB_USERS, "WHERE user_empresa = :id AND user_id = :user", "id={$idemp}&user={$id}");
if (!$Read->getResult()):
  $jSon['error'] = true;
elseif ($id == $_SESSION['userlogin']['user_id']):
  $jSon["result"] = 0;
elseif ($_SESSION['userlogin']['user_level'] >= 5):
  $read = $Read->getResult();
  if ($read[0]['user_level'] == 5):
    $Read->ExeRead(TB_USERS, "WHERE user_id != :id AND user_level = :lv AND user_empresa = :emp", "id={$id}&lv={$level}&emp={$idemp}");
    if (!$Read->getRowCount()):
      $jSon["result"] = 1;
    else:
      $Delete->ExeDelete(TB_USERS, "WHERE user_id = :id", "id={$id}");
      if (!$Delete->getResult()):
        $jSon['error'] = true;
      else:
        $read = $Read->getResult();
        $delCapa = $read[0]['user_cover'];
        if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
          unlink("../../uploads/{$delCapa}");
      endif;
      $jSon = true;
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Removeu o usuário {$read[0]['user_name']} {$read[0]['user_lastname']}", date("Y-m-d H:i:s"));
    endif;
  endif;
else:
  $Delete->ExeDelete(TB_USERS, "WHERE user_id = :id", "id={$id}");
  if (!$Delete->getResult()):
    $jSon['error'] = true;
  else:
    $jSon = true;
    $read = $Read->getResult();
    $delCapa = $read[0]['user_cover'];
    if (file_exists("../../uploads/{$delCapa}") && !is_dir("../../uploads/{$delCapa}")):
      unlink("../../uploads/{$delCapa}");
  endif;
  Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Removeu o usuário {$read[0]['user_name']} {$read[0]['user_lastname']}", date("Y-m-d H:i:s"));
endif;
endif;
else:
  $jSon['result'] = 2;
endif;
break;
case 'removeRelatorio':
$id = (int) $post['itemId'];
if ($_SESSION['userlogin']['user_level'] < 4):
  $jSon['result'] = 0;
else:
  $Read->ExeRead(TB_RELATORIOS, "WHERE relat_id = :id AND user_empresa = :emp", "id={$id}&emp={$_SESSION['userlogin']['user_empresa']}");
  if (!$Read->getResult()):
    $jSon["error"] = true;
  else:
    $file = $Read->getResult();
    $file = $file[0]['relat_file'];
    $Delete->ExeDelete(TB_RELATORIOS, "WHERE relat_id = :id AND user_empresa = :emp", "id={$id}&emp={$_SESSION['userlogin']['user_empresa']}");
    if (!$Delete->getResult()):
      $jSon['error'] = true;
    else:
      if (file_exists("../../uploads/{$file}") && !is_dir("../../uploads/{$file}")):
        unlink("../../uploads/{$file}");
      $jSon = true;
    else:
      $jSon['result'] = 1;
    endif;
    Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Relatório de consultoria", "Deletou o relatório #{$file[0]['relat_id']} De: {$file[0]['relat_datarelat']}", date("Y-m-d H:i:s"));
  endif;
endif;
endif;
break;
case 'removeNota':
$id = (int) $post['itemId'];
if ($_SESSION['userlogin']['user_level'] < 3):
  $jSon['result'] = 0;
else:
  $Read->ExeRead(TB_NOTAS, "WHERE notas_id = :id AND user_empresa = :emp", "id={$id}&emp={$_SESSION['userlogin']['user_empresa']}");
  if (!$Read->getResult()):
    $jSon["error"] = true;
  else:
    $Delete->ExeDelete(TB_NOTAS, "WHERE notas_id = :id AND user_empresa = :emp", "id={$id}&emp={$_SESSION['userlogin']['user_empresa']}");
    if (!$Delete->getResult()):
      $jSon['error'] = true;
    else:
      $read = $Read->getResult();
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notas", "Deletou a nota #{$read[0]['notas_id']} Titulo: {$read[0]['notas_titulo']}", date("Y-m-d H:i:s"));
    endif;
  endif;
endif;
break;
default:
$jSon['error'] = "Erro ao selecionar a ação!";
endswitch;
echo json_encode($jSon);