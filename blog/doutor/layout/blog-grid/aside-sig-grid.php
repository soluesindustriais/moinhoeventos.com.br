<form>
	<input type="hidden" class="j_url" value="<?= RAIZ ?>/<?= $getURL; ?>" />
</form>
<aside class="aside-sig-grid">
	<form action="<?=$url?>pesquisa" method="post" class="search aside-sig-search">
		<input type="text" placeholder="Buscar por ..." name="palavra" id="Buscar">
		<button type="submit" aria-label="Buscar"><i class="fas fa-search"></i></button>
	</form>

	<? include('inc/blog-recent-posts.php'); ?>

	<div class="aside-sig-menu">
		<?php include 'inc/sub-menu-aside.php'; ?>
	</div>

	<? include('inc/blog-tags-inc.php'); ?>
</aside>