<div class="blog-inc htmlchars">
	<?php
	$Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats AND blog_name = :nm LIMIT 0, 8", "stats=2&nm={$lastCategory}");
	if ($Read->getResult()) :
		foreach ($Read->getResult() as $dados) :
			extract($dados);
	?>
			<div class="container pt-0">
				<!-- <div class="blog-inc__cover">
					<img src="<?= RAIZ . '/doutor/uploads/' . $blog_cover; ?>" title="<?= $blog_title; ?>" alt="<?= $blog_title; ?>" />
				</div -->

				<div class="blog-inc__info">
					<div class="blog-inc__heading">
						<?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
						$blogDay = $newDate[0];
						$blogMonth = $newDate[1];
						$blogYear = $newDate[2];

						$authorKey = array_search($user_id, array_column($authors, 'user_id'));
						$itemAuthor = $authors[$authorKey]['user_name'];
						?>
						<p class="blog-inc__heading-text">Por: <a href="<?= $url ?>autor/<?= urlencode($itemAuthor) ?>" rel="nofollow" title="<?= $itemAuthor ?>"><?= $itemAuthor ?></a> - <?= $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear ?></p>
					</div>

					<div class="blog-inc__content">
						<?php
						$blog_content = preg_replace('/(src=")(.*)(\/doutor\/uploads\/2\/)/mU', '$1' . RAIZ . '$3', $blog_content);
						$blog_content = str_replace('src="doutor', 'src="' . RAIZ . '/doutor', $blog_content);
						echo $blog_content;
						?>
					</div>

					<?php if (strlen($blog_keywords) > 1) : ?>
						<div class="blog-tag-list">
							<p class="fw-bold">Tags:</p>
							<?php $blogTagList = explode(",", $blog_keywords);
							foreach ($blogTagList as $key => $item) : ?>
								<a href="<?= $url ?>tags/<?= queryFilter($item) ?>" title="<?= $item ?>"><?= $item ?></a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php
			$Read->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND cat_parent = :cat", "id={$blog_id}&cat={$cat_parent}");
			if ($Read->getResult()) :
			?>
				<h3>Confira mais imagens:</h3>
				<div class="blog-inc__gallery">
					<?php foreach ($Read->getResult() as $gallery) : extract($gallery); ?>
						<div class="gallery__item"><a href="<?= RAIZ . '/doutor/uploads/' . $gallery_file; ?>" data-fancybox="group1" class="lightbox" title="<?= $blog_title; ?>"><?= Check::Image('doutor/uploads/' . $gallery_file, $blog_title, null, 250, 150) ?></a></div>
					<?php endforeach; ?>
				</div>
	<?php
			endif;
		endforeach;
	endif; ?>
</div> <!-- htmlchars -->

<script>
	<? include('slick/slick.min.js'); ?>
	$(document).ready(function() {
		$('.blog-inc__gallery').slick({
			autoplaySpeed: 3000,
			autoplay: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			cssEase: 'ease',
			dots: false,
			arrows: true,
			responsive: [{
				breakpoint: 767,
				settings: {
					slidesToShow: 1
				}
			}]
		});
	});
</script>