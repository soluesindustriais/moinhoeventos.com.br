<?php

/**
 * Upload.class [ HELPER ]
 * Reponsável por executar upload de imagens, arquivos e mídias no sistema!
 * 
 * @copyright (c) Rafael da Silva Lima & Doutores da Web
 */
class Upload {

  private $File;
  private $Name;
  private $Send;

  /** IMAGE UPLOAD */
  private $Width;
  private $Image;

  /** RESULTSET */
  private $Result;
  private $Error;

  /** DIRETÓTIOS */
  private $SubFolder;
  private $Folder;
  private static $BaseDir;

  /**
   * Verifica e cria o diretório padrão de uploads no sistema!<br>
   * <b>uploads/</b>
   */
  function __construct($BaseDir = null) {
    self::$BaseDir = ( (string) $BaseDir ? $BaseDir : 'uploads/');
    if (!file_exists(self::$BaseDir) && !is_dir(self::$BaseDir)):
      mkdir(self::$BaseDir, 0777);
    endif;
  }

  /**
   * <b>Enviar Imagem:</b> Basta envelopar um $_FILES de uma imagem e caso queira um nome e uma largura personalizada.
   * Caso não informe a largura será 1024!
   * @param FILES $Image = Enviar envelope de $_FILES (JPG ou PNG)
   * @param STRING $Name = Nome da imagems ( ou do artigo )
   * @param INT $Width = Largura da imagem ( 1024 padrão )
   * @param STRING $Folder = Pasta personalizada
   */
  public function Image(array $Image, $Name = null, $Width = null, $SubFolder = null, $Folder = null) {
    $this->File = $Image;
    $this->Name = ( (string) $Name ? $Name : substr($Image['name'], 0, strrpos($Image['name'], '.')) );
    $this->Width = ( (int) $Width ? $Width : 1024 );
    $this->SubFolder = ( (int) $SubFolder ? $SubFolder : 0 );
    $this->Folder = ( (string) $Folder ? $Folder : 'images' );


    $this->CheckFolder($this->SubFolder, $this->Folder);
    $this->setFileName();
    $this->UploadImage();
  }

  /**
   * <b>Enviar Arquivo:</b> Basta envelopar um $_FILES de um arquivo e caso queira um nome e um tamanho personalizado.
   * Caso não informe o tamanho será 2mb!
   * @param FILES $File = Enviar envelope de $_FILES (PDF ou DOCX)
   * @param STRING $Name = Nome do arquivo ( ou do artigo )
   * @param STRING $Folder = Pasta personalizada
   * @param STRING $MaxFileSize = Tamanho máximo do arquivo (2mb)
   */
  public function File(array $File, $Name = null, $SubFolder = null, $Folder = null, $MaxFileSize = null) {
    $this->File = $File;
    $this->Name = ( (string) $Name ? $Name : substr($File['name'], 0, strrpos($File['name'], '.')) );
    $this->SubFolder = ( (int) $SubFolder ? $SubFolder : 0 );
    $this->Folder = ( (string) $Folder ? $Folder : 'files' );
    $MaxFileSize = ( (int) $MaxFileSize ? $MaxFileSize : 2 );

    $FileAccept = array(
      'application/pdf',
      'application/excel',
      "application/octet-stream",
      "application/vnd.ms-excel",
      "application/x-csv",
      "application/csv",
      'application/msword',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'binary/octet-stream'
    );

    if ($this->File['size'] > ($MaxFileSize * (1024 * 1024))):
      $this->Result = false;
      $this->Error = "Arquivo muito grande, tamanho máximo permitido de {$MaxFileSize}mb.";
    elseif (!in_array($this->File['type'], $FileAccept)):
      $this->Result = false;
      $this->Error = 'Tipo de arquivo não suportado. Envie .PDF, .XLS, .DOC ou .CSV!';
    elseif (!$this->getStorage()):
      $this->Result = false;
    else:
      $this->CheckFolder($this->SubFolder, $this->Folder);
      $this->setFileName();
      $this->MoveFile();
    endif;
  }

  /**
   * <b>Enviar Mífia:</b> Basta envelopar um $_FILES de uma mídia e caso queira um nome e um tamanho personalizado.
   * Caso não informe o tamanho será 40mb!
   * @param FILES $Media = Enviar envelope de $_FILES (MP3 ou MP4)
   * @param STRING $Name = Nome do arquivo ( ou do artigo )
   * @param STRING $Folder = Pasta personalizada
   * @param STRING $MaxFileSize = Tamanho máximo do arquivo (40mb)
   */
  public function Media(array $Media, $Name = null, $Folder = null, $MaxFileSize = null) {
    $this->File = $Media;
    $this->Name = ( (string) $Name ? $Name : substr($Media['name'], 0, strrpos($Media['name'], '.')) );
    $this->Folder = ( (string) $Folder ? $Folder : 'medias' );
    $MaxFileSize = ( (int) $MaxFileSize ? $MaxFileSize : 40 );

    $FileAccept = array(
      'audio/mp3',
      'video/mp4'
    );

    if ($this->File['size'] > ($MaxFileSize * (1024 * 1024))):
      $this->Result = false;
      $this->Error = "Arquivo muito grande, tamanho máximo permitido de {$MaxFileSize}mb.";
    elseif (!in_array($this->File['type'], $FileAccept)):
      $this->Result = false;
      $this->Error = 'Tipo de arquivo não suportado. Envie audio MP3 ou vídeo MP4!';
    else:
      $this->CheckFolder($this->Folder);
      $this->setFileName();
      $this->MoveFile();
    endif;
  }

  /**
   * <b>Verificar Upload:</b> Executando um getResult é possível verificar se o Upload foi executado ou não. Retorna
   * uma string com o caminho e nome do arquivo ou FALSE.
   * @return STRING  = Caminho e Nome do arquivo ou False
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Obter Erro:</b> Retorna um array associativo com um code, um title, um erro e um tipo.
   * @return ARRAY $Error = Array associatico com o erro
   */
  public function getError() {
    return $this->Error;
  }

  /*
   * ***************************************
   * **********  PRIVATE METHODS  **********
   * ***************************************
   */

  //Verifica e cria os diretórios com base em tipo de arquivo, ano e mês!
  private function CheckFolder($SubFolder, $Folder) {
    list($y, $m) = explode('/', date('Y/m'));
    $this->CreateFolder("{$SubFolder}");
    $this->CreateFolder("{$SubFolder}/{$Folder}");
    $this->CreateFolder("{$SubFolder}/{$Folder}/{$y}");
    $this->CreateFolder("{$SubFolder}/{$Folder}/{$y}/{$m}/");
    $this->Send = "{$SubFolder}/{$Folder}/{$y}/{$m}/";
  }

  //Verifica e cria o diretório base!
  private function CreateFolder($SubFolder) {
    if (!file_exists(self::$BaseDir . $SubFolder) && !is_dir(self::$BaseDir . $SubFolder)):
      mkdir(self::$BaseDir . $SubFolder, 0777);
    endif;
  }

  //Verifica e monta o nome dos arquivos tratando a string!
  private function setFileName() {
    $FileName = Check::Name($this->Name) . strrchr($this->File['name'], '.');
    if (file_exists(self::$BaseDir . $this->Send . $FileName)):
      $FileName = Check::Name($this->Name) . '-' . time() . strrchr($this->File['name'], '.');
    endif;
    $this->Name = $FileName;
  }

  //Realiza o upload de imagens redimensionando a mesma!
  private function UploadImage() {
    //Verifica o real mimetype do arquivo
    $type = getimagesize($this->File['tmp_name']);
    $type = $type['mime'];

    switch ($type):
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/pjpeg':
        $this->Image = imagecreatefromjpeg($this->File['tmp_name']);
        break;
      case 'image/png':
      case 'image/x-png':
        $this->Image = imagecreatefrompng($this->File['tmp_name']);
        break;
      case 'image/webp':
        $this->Image = imagecreatefromwebp($this->File['tmp_name']);
    endswitch;

    if (!$this->Image):
      $this->Result = false;
      $this->Error = 'Tipo de arquivo inválido, envie imagens JPG ou PNG.';
    else:
      $x = imagesx($this->Image);
      $y = imagesy($this->Image);
      $ImageX = ( $this->Width < $x ? $this->Width : $x );
      $ImageH = ($ImageX * $y) / $x;

      $NewImage = imagecreatetruecolor($ImageX, $ImageH);
      imagealphablending($NewImage, false);
      imagesavealpha($NewImage, true);
      imagecopyresampled($NewImage, $this->Image, 0, 0, 0, 0, $ImageX, $ImageH, $x, $y);

      switch ($type):
        case 'image/jpg':
        case 'image/jpeg':
        case 'image/pjpeg':
          imagejpeg($NewImage, self::$BaseDir . $this->Send . $this->Name);
          break;
        case 'image/png':
        case 'image/x-png':
          imagepng($NewImage, self::$BaseDir . $this->Send . $this->Name);
          break;
        case 'image/webp':
          imagewebp($NewImage, self::$BaseDir . $this->Send . $this->Name);
      endswitch;

      if (!$NewImage):
        $this->Result = false;
        $this->Error = 'Tipo de arquivo inválido, envie imagens JPG ou PNG.';
      else:
        $this->Result = $this->Send . $this->Name;
        $this->Error = null;
      endif;

      imagedestroy($this->Image);
      imagedestroy($NewImage);
      $this->PushToRepo();
    endif;
  }

  //Verifica o espaço em disco utilizado
  private function getStorage() {
    $Read = new Read;
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$this->SubFolder}");
    if ($Read->getResult()):
      $read = $Read->getResult();
      $permitido = $read[0]['empresa_disco'] . "000";
      $permitido = (int) $permitido;
      $emuso = Check::GetDirectorySize($this->SubFolder);
      $totaladd = $emuso + $this->File['size'];

      if ($totaladd > $permitido):
        $this->Error = 'Não é possível enviar este arquivo, pois o mesmo ultrapassará o limite de espaço em disco disponível do seu plano.';
        return false;
      elseif ($emuso >= $permitido):
        $this->Error = 'Você atingiu o limite de espaço em disco disponível do seu plano.';
        return false;
      else:
        return true;
      endif;

    endif;
  }

  //Envia arquivos e mídias
  private function MoveFile() {
    if (move_uploaded_file($this->File['tmp_name'], self::$BaseDir . $this->Send . $this->Name)):
      $this->Result = $this->Send . $this->Name;
      $this->Error = null;
      $this->PushToRepo();
    else:
      $this->Result = false;
      $this->Error = 'Erro ao mover o arquivo. Por favor tente mais tarde.';
    endif;
  }
  
  private function PushToRepo(){
    if(strpos($_SERVER['HTTP_HOST'], 'localhost') === false && strpos($_SERVER['HTTP_HOST'], 'mpitemporario') === false){
      shell_exec("cd .. && git add . && git commit -m '[auto-push] Changes made on SIG by the client' && git push origin master");
    }
  }

}
