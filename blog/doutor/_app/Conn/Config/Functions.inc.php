<?php

function GetPendenciaTipo($Nome) {
    $Nome = explode(':', $Nome);
    if ($Nome[0] == 'Regularização'):
        return 'regularizacao';
    elseif ($Nome[0] == 'Outorga'):
        return 'outorgas';
    endif;
}

//CSS para tipos de cores nos botões de status
function getStatusCSS($Status) {
    $css = [
        1 => 'success',
        2 => 'danger',
        3 => 'warning',
        4 => 'primary',
        5 => 'black',
        6 => 'purple',
        7 => 'info',
        8 => 'default',
    ];
    return $css[$Status];
}

//Status das outorgas de água
function getStatusOp($Situacao = null) {
    $status = [
        1 => 'Válida',
        2 => 'Vencida',
        3 => 'À vencer',
        4 => 'À vigorar',
        5 => 'Vence hoje',
        6 => 'Renovada',
        7 => 'Cancelada',
    ];
    if (!empty($Situacao)):
        return $status[$Situacao];
    else:
        return $status;
    endif;
}

//Status das reg e out vencidas ou renovadas
function getStatusManual($Situacao = null) {
    $status = [
        1 => 'Renovada',
        2 => 'Cancelada',
        3 => 'Validar pela data'
    ];
    if (!empty($Situacao)):
        return $status[$Situacao];
    else:
        return $status;
    endif;
}

//Status de empresas
function getStatusEmp($Status = null) {
    $StatusEmp = [
        1 => 'Ativa',
        2 => 'Desativada',
        3 => 'Cancelada',
    ];

    if (!empty($Status)):
        return $StatusEmp[$Status];
    else:
        return $StatusEmp;
    endif;
}

//categorias das obrigações legais
function getCategObrigacoes($ObrigId = null) {
    $Status = [
        1 => 'Barragens',
        2 => 'PAV',
        3 => 'Monitoramentos ambientais',
        4 => 'Inventário de resíduos sólidos',
        5 => 'IBAMA',
        6 => 'Relatório de atividades industriais',
        7 => 'CAR',
    ];

    if (!empty($ObrigId)):
        return $Status[$ObrigId];
    else:
        return $Status;
    endif;
}

//categorias das regualizações ambientais
function getCategRegularizacao($RegId = null) {
    $Status = [
        1 => 'Industrial',
        2 => 'Agrícola',
        3 => 'Posto de abastecimento',
    ];

    if (!empty($RegId)):
        return $Status[$RegId];
    else:
        return $Status;
    endif;
}

//categorias das legislações ambientais
function GetCategLegislacao($LegEsferaId = null) {
    $Status = [
        1 => 'Municipal',
        2 => 'Estadual',
        3 => 'Federal',
        4 => 'Nacional',
        5 => 'Internacional',
    ];

    if (!empty($LegEsferaId)):
        return $Status[$LegEsferaId];
    else:
        return $Status;
    endif;
}

//Nomenclatura dos niveis de acesso geral
function GetNiveis($NumNivel = null) {
    $Desc = [
        1 => 'Nível 1 - Colaborador',
        2 => 'Nível 2 - Supervisor',
        3 => 'Nível 3 - Gestor',
        4 => 'Nível 4 - Consultor',
        5 => 'Nível 5 - Diretor',
    ];

    if (!empty($NumNivel)):
        return $Desc[$NumNivel];
    else:
        return $Desc;
    endif;
}

//Nomenclatura dos niveis de acesso usuários
function GetNiveisUser($NumNivel = null) {
    $Desc = [
        1 => 'Nível 1 - Colaborador',
        2 => 'Nível 2 - Supervisor',
        3 => 'Nível 3 - Gestor',
    ];

    if (!empty($NumNivel)):
        return $Desc[$NumNivel];
    else:
        return $Desc;
    endif;
}

//Nomenclatura dos niveis de acesso usuários
function GetNiveisFunc($NumNivel = null) {
    $Desc = [
        4 => 'Nível 4 - Consultor',
        5 => 'Nível 5 - Diretor',
    ];

    if (!empty($NumNivel)):
        return $Desc[$NumNivel];
    else:
        return $Desc;
    endif;
}

//Nomenclatura das categoria de condicionates
function GetCategCondicionate($categId = null) {
    $Desc = [
        1 => 'Outorgas de água',
        2 => 'Regularização ambiental'
    ];

    if (!empty($categId)):
        return $Desc[$categId];
    else:
        return $Desc;
    endif;
}
