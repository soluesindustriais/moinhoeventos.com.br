<?php

$categorizedMPIs = false;

include_once($DIR."inc/vetKey.php");
//Exemplo de categorização no arquivo: vetKeyCategory.php, basta definir o ID da categoria de cada MPI
//Defina os nomes e outras informações das categorias no arquivo vetCategories.php

if($categorizedMPIs): //MPI Categorizada
    include_once($DIR.'inc/vetCategories.php');
    foreach($vetKeyCat as $itemCat): ?>        
        <li class="dropdown"><a href="<?=$urlBase.$itemCat['url']?>" title="<?=$itemCat['title']?>"><?=$itemCat['title']?></a>
            <ul class="sub-menu">
                <?php foreach($vetKey as $item) { 
                    if($item['cat_id'] === $itemCat['id']): ?>
                    <li><a data-mpi href="<?=$urlBase.$item['url']?>" title="<?=$item['key']?>"><?=$item['key']?></a></li>
                <?php endif; } ?>             
            </ul>
        </li>   
<?php endforeach;

else: //MPI Padrão
    foreach ($vetKey as $key => $vetor): ?>
    <li>
        <a data-mpi href="<?=$urlBase.$vetor['url']?>" title="<?=$vetor['key']?>"><?=$vetor['key']?></a>
    </li>
    <? endforeach;
endif; ?>