<?php
//Montagem da busca
$search = filter_input(INPUT_POST, 'busca', FILTER_DEFAULT);
if (!empty($search)):
  $search = strip_tags(trim(urlencode($search)));
  header('Location: ' . RAIZ . '/pesquisa-sig/' . $search);
endif;
//Inclusão da geral com dados e variaveis do cliente
include('inc/geral.php');
//Verificação se a categoria ou página existe no banco de dados, se existir será carregado o head-seo com as infos vindo do Objeto SEO.
$pagina = false;
if (isset($URL) && !in_array('', $URL)):
//Armazena sempre o ultimo item da url
  $lastCategory = end($URL);
foreach ($URL as $paginas => $value):
  if (!empty($value)):
    $Read->ExeRead(TB_PAGINA, "WHERE pag_name = :nm AND pag_status = :status", "nm={$value}&status=2");
    if ($Read->getResult()):
      $pagina = true;
    endif;
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :status", "nm={$value}&status=2");
    if ($Read->getResult()):
      $pagina = true;
    endif;
  endif;
endforeach;
endif;
if (!$pagina):
  ?>
  <!DOCTYPE html>
  <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
      <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
        <!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
          <head>
            <!-- HEAD REPOSÁVEL PELAS PÁGINAS ESTÁTICAS -->
            <meta charset="utf-8">
            <title><?= $title . " - " . $nomeSite ?></title>
            <base href="<?= RAIZ; ?>">
            <?php
            
            if(strlen($desc) < 140){$desc = $auto_desc;}
            
            // clean description 
            $desc = strip_tags($desc);
            $desc = str_replace('  ', ' ', $desc);
            $desc = str_replace(' ,', ',', $desc);
            $desc = str_replace(' .', '.', $desc);
            $desc = str_replace(' ?', '?', $desc);
            if (mb_strlen($desc,"UTF-8") > 160) {
              $desc = mb_substr($desc, 0, 159);
              $finalSpace = strrpos($desc, " ");
              $desc = substr($desc, 0, $finalSpace);
              $desc .= ".";
            }else if (mb_strlen($desc,"UTF-8") < 140 && mb_strlen($desc,"UTF-8") > 130 ) {
              $desc .= "... Saiba mais.";
            }
            ?>
            <meta name="description" content="<?= $desc ?>">
            <meta name="keywords" content="<?=str_replace($prepos,', ', $h1).', '.$nomeSite?>">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="geo.position" content="<?=GetCoordinates($mapa);?>">
            <meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>">
            <meta name="geo.region" content="<?= $UF ?>-BR">
            <meta name="ICBM" content="<?=GetCoordinates($mapa);?>">
            <meta name="robots" content="index,follow">
            <meta name="rating" content="General">
            <meta name="revisit-after" content="7 days">
            <link rel="canonical" href="<?= RAIZ . '/' . $urlPagina; ?>">
            <?php
            if (empty($author)):
              echo '<meta name="author" content="' . $nomeSite . '">';
            else:
              echo '<link rel="author" href="' . $author . '">';
            endif;
            ?>
            <link rel="shortcut icon" href="<?= $urlBase ?>imagens/favicon.png">
            <meta property="og:region" content="Brasil">
            <meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
            <meta property="og:type" content="article">
            <?php if (file_exists($url . $pasta . $urlPagina . "-01.jpg")): ?>
              <meta property="og:image" content="<?= RAIZ . '/' . $pasta . $urlPagina ?>-01.jpg">
            <?php endif; ?>
            <meta property="og:url" content="<?= RAIZ . '/' . $urlPagina ?>">
            <meta property="og:description" content="<?= $desc ?>">
            <meta property="og:site_name" content="<?= $nomeSite ?>">
            <meta name="format-detection" content="telephone=no">
            <?php
          else:
            require 'inc/head-seo.php';
          endif; ?>

          <!-- ABAIXO DESSA LINHA INSERIR OS ARQUIVOS QUE IRAM ATUAR EM TODAS PÁGINAS (ESTÁTICAS E COM SIG) -->

          <?php include('inc/jquery.php')?>
          <script>
            <?
            include ("js/default-passive-events.js");
            include ("js/header-scroll.js");
            if($isMobile): 
              include("js/menu-hamburger.js");
            endif;
            if($isLocalhost):
              include("js/check-html.js");
            endif;   
            include ("doutor/vendors/bootstrap-sweetalert/lib/sweet-alert.min.js");
            include ("js/ajax.js");
            ?>
          </script>
          <style>
            <?
            include ("css/normalize.css");
            include ("css/doutor.css");
            include ("css/style-base-blog.css");
            include ("css/style-blog.css");            
            include ("doutor/vendors/bootstrap-sweetalert/lib/sweet-alert.css");
            ?>
          </style>

          <?php if (isset($pagInterna)) : ?>
              <style>
                  <? include("css/mpi-1.0.css"); ?>
              </style>
          <?php endif; ?>

          <?php if ($isMobile) : ?>
              <style>
                  <? include("css/menu-hamburger.css"); ?>
              </style>
          <?php endif; ?>

                    
          <!-- Google Fonts Call CSS Generator -->
          <?=GoogleFontsStyleGenerator($fontFamily);?>

          <!-- Google Fonts Tag Generator -->
          <?=GoogleFontsTagGenerator($fontFamily, $weights, $italicWeights);?>

          <link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" onload="this.rel='stylesheet'">

          <? include('inc/form-scripts.php'); ?>
          <? include('inc/whatsapp-button-post.php'); ?>
