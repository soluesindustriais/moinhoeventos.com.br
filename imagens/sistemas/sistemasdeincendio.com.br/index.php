<?
$h1         = 'Aluguel de Equipamento';
$title      = 'Aluguel de Equipamentos | Cotações gratuitas';
$desc       = 'Faça cotações para aluguel de equipamentos em apenas um lugar, aqui na Aluguel de Equipamentos';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>

</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1><?=$h1?></h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Aluguel de betoneira</h2>
        <p>O aluguel de betoneira é super indicado para a preparação de concreto com muita eficiência e agilidade durante a execução de obras. O equipamento passa por um processo de manutenção preventivo, visando garantir seu perfeito funcionamento na obra, todas as máquinas são equipadas com sistema de segurança normatizado tais como proteção do pinhão e cremalheira e caixa de chave liga e desliga com botão de emergência.</p>
        <a class="cd-btn btn-intro">Fazer cotação</a>
        <a href="<?=$url?>aluguel-de-betoneira" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Aluguel de compressor</h2>
        <p>Os aluguel de compressor disponibiliza o produto ideal para pinturas na construção civil usando tinta látex, óleo e verniz. O equipamento pulveriza a tinta com pressão sobre o local desejado conferindo um resultado uniforme com agilidade, além de muitas outras vantagens, tais como excelente custo benefício, assistência técnica autorizada para revisar os equipamentos locados e contrato e condições facilitadas.</p>
        <a class="cd-btn btn-intro">Fazer cotação</a>
        <a href="<?=$url?>aluguel-de-compressor" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Aluguel de ferramentas</h2>
        <p>O aluguel de ferramentas precisa ser feito de forma ágil e eficiente, disponibilizando equipamentos de grande eficiência, para que os consumidores consigam realizar suas obras da melhor maneira possível. Muitos produtos são necessários na construção, como por exemplo betoneiras e marteletes.</p>
        <a class="cd-btn btn-intro">Fazer cotação</a>
        <a href="<?=$url?>aluguel-de-ferramentas" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main> 
  <section class="wrapper-main" data-anime="in">
    <div class="main-center">
      <div class=" quadro-2 ">
       <div class="child-main">
         <h2>Central de alarme de incêndio</h2>
         <div class="div-img">
           <p >Inúmeros equipamentos compõem as ferramentas de uma construção, cada um direcionado para uma atividade específica, como os alicates, amplamente utilizadas pelos profissionais no canteiro de obras, eles se dividem nas seguintes categorias: alicate universal, alicate de pressão, alicate de corte frontal, alicate de corte diagonal e alicate de bico meia cana.</p>
         </div>
    
      <div class="child-2-main">    
        <div class=" incomplete-box" data-anime="up">
           <ul data-anime="in">
             <li>
               <p>Cada obra possui necessidades específicas, que demandam a locação de equipamentos diferenciados para vencer os mais diversos desafios, e eles precisam estar em seu perfeito estado, evitando toda e qualquer falha de uso. Há, porém, alguns materiais que são indispensáveis em qualquer canteiro de obras, tais como:</p>
              <li><i class="fas fa-angle-right"></i>Alicates;</li>
             <li><i class="fas fa-angle-right"></i>Arcos de serra;</li>
             <li><i class="fas fa-angle-right"></i>Betoneiras;</li>
             <li><i class="fas fa-angle-right"></i>Chaves;</li>
             <li><i class="fas fa-angle-right"></i>Compressor de ar;</li>
             <li><i class="fas fa-angle-right"></i>Entre outros.</li>
           </ul>
           <span class="btn-4" data-anime="up"> Orçamento Grátis </span>
         </div>
        <div class="child-p"data-anime="right">
         <p>Há muitas outras situações em que a Locação de compactador, por exemplo, pode ser mais econômico do que a compra de um equipamento novo.</p> 
          <img src="<?=$url?>imagens/img-home/central-de-alarme.jpg" alt="Central de Alarme" title="Central de Alarme">        
         </div>
      </div>
       </div> 
      </div>
    </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-wrench fa-flip-horizontal fa-7x" ></i>
            <div>
              <p>Grande variedade de Ferramentas</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-cogs fa-7x"></i>
            <div>
              <p>Manuteção de Equipamentos</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-screwdriver fa-flip-horizontal fa-7x"></i>
            <div>
              <p>Equipamentos para locação</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>alarme-de-incendio">
              <div class="fig-img" data-anime="in">
                <h2>Alarme de incêndio</h2>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>sistema-de-combate-a-incendio">
              <div class="fig-img2" data-anime="in">
                <h2>Sistema de combate a incêndio</h2>
                
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>equipamento-de-combate-a-incendio">
              <div class="fig-img" data-anime="in">
                <h2>Equipamentos de combate a incêndio</h2>  
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/brigada-de-incendio.jpg" class="lightbox" title="incendios">
              <img src="<?=$url?>imagens/thumbs/brigada-de-incendio.jpg" alt="incêndios" title="incêndios">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/comprar-extintor.jpg" class="lightbox" title="Comprar Extintor">
              <img src="<?=$url?>imagens/thumbs/comprar-extintor.jpg" alt="Comprar Extintor" title="Comprar Extintor">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/curso-antincendio.jpg" class="lightbox" title="Tudo contra incêndios">
              <img src="<?=$url?>imagens/thumbs/curso-antincendio.jpg" alt="Tudo contra incêndios" title="Tudo contra incêndios">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/detector-de-incendio.jpg" class="lightbox" title="Detector de Incêndio">
              <img src="<?=$url?>imagens/thumbs/detector-de-incendio.jpg" alt="Detector de Incêndio" title="Detector de Incêndio">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/extintores-de-incendio.jpg" class="lightbox" title="Extintores de Incêndio">
              <img src="<?=$url?>imagens/thumbs/extintores-de-incendio.jpg" alt="Extintores de Incêndio" title="Extintores de Incêndio">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/extintor-para-carro.jpg" class="lightbox" title="Extintor para Carro">
              <img src="<?=$url?>imagens/thumbs/extintor-para-carro.jpg" alt="Extintor para Carro" title="Extintor para Carro">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/rede-incendio.jpg" class="lightbox" title="Rede Incêndio">
              <img src="<?=$url?>imagens/thumbs/rede-incendio.jpg" alt="Rede Incêndio" title="Rede Incêndio">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/sistema-contra-incendio.jpg" class="lightbox" title="Sistema Contra Incêndio">
              <img src="<?=$url?>imagens/thumbs/sistema-contra-incendio.jpg" alt="Sistema Contra Incêndio" title="Sistema Contra Incêndio">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/sistema-de-incendio.jpg" class="lightbox" title="Sistema de Incêndio">
              <img src="<?=$url?>imagens/thumbs/sistema-de-incendio.jpg" alt="Sistema de Incêndio" title="Sistema de Incêndio">
              </a>
            </li>
            <li><a href="<?=$url?>imagens/tudo-para-incendio.jpg" class="lightbox" title="Tudo para Incêndio">
              <img src="<?=$url?>imagens/thumbs/tudo-para-incendio.jpg" alt="Tudo para Incêndio" title="Tudo para Incêndio">
              </a>
            </li>

          </ul>
</div>
</div>
</section>
<? include('inc/form-mpi.php'); ?>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" media="screen">
<script  src="<?=$url?>nivo/jquery.nivo.slider.js"></script>
<script >
$(window).load(function() {
$('#slider').nivoSlider();
});
</script>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>

</body>
</html>


