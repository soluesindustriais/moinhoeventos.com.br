<div class="clear"></div>
<footer>
  <div class="wrapper-content2">
    <div class="contact-footer">
      <address>
        <span><?= $nomeSite . " - " . $slogan ?></span>
      </address>
      <?= $ddd ?> <strong><?= $fone ?></strong>
    </div>
    <div class="menu-footer">
      <nav>
        <ul>
          <li><a rel="nofollow" href="<?= $url ?>" title="Página inicial">Home</a></li>
          <li><a rel="nofollow" href="<?= $url ?>informacoes" title="Informações">Informações</a></li>
          <li><a rel="nofollow" href="<?= $url ?>empresa" title="Empresa <?= $nomeSite ?>">Empresa</a></li>
          <li><a href="<?= $url ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
        </ul>
      </nav>
    </div>
    <? include('inc/canais.php'); ?>
    <br class="clear" />
    <div class="copyright-footer">
      Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
      <div class="selos">
        <a rel="nofollow" href="https://validator.w3.org/check?uri=<?= $url . $urlPagina ?>" target="_blank" title="Site Desenvolvido em HTML5 nos padrões internacionais W3C"><img src="<?= $url ?>imagens/selo-w3c-html5.png" alt="Site Desenvolvido em HTML5 nos padrões internacionais W3C" /></a>
        <a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $url . $urlPagina ?>" target="_blank" title="Site Desenvolvido nos padrões internacionais W3C"><img src="<?= $url ?>imagens/selo-w3c-css.png" alt="Site Desenvolvido nos padrões W3C" /></a>
        <a href="https://www.solucoesindustriais.com.br/empresa/instalacoes_e_equipamento_industrial/rp-truck/produtos/todos" target="_blank" title="Soluções Industriais">
          <img src="https://www.solucoesindustriais.com.br/images/logo/logo-solucoes-industriais-black.png" alt="Soluções Industriais" title="Soluções Industriais" />
        </a>
      </div>
    </div>
  </div>
  <style media="screen">
    .modal-contato-itens div.descricao {
      width: 100% !important;
    }
  </style>
  <!-- Shark Orcamento -->
  <div id="sharkOrcamento" style="display: none;"></div>
  <!-- <script src="https://www.solucoesindustriais.com.br/assets/js/plugin/orcamentos.min.js"></script>
  <script>
    pluginOrcamentoJs.init({
      industria: 'solucoes-industriais',
      btnOrcamento: '.botao-cotar',
      titulo: '#sharkOrcamento'
    });
    $('.botao-cotar').on('click', function() {
      var title = $(this).attr('title');
      $('#sharkOrcamento').html(title.trim());
    });
  </script> -->
</footer>
<!-- MENU  MOBILE -->
<script async src="<?= $url ?>js/jquery.slicknav.js"></script>
<script>
  (function() {
    var po = document.createElement('script');
    po.type = 'text/javascript';
    po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js?onload=OnLoadCallback';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
  })();
</script>
<script defer src="<?= $url ?>js/geral.js"></script>
<!-- Plugin facebook -->

<div>
  <app-cotacao-solucs appConfig='{"btnOrcamento": ".botao-cotar", "titulo": "h2", "industria": "solucoes-industriais", "container":"nova-api" }'></app-cotacao-solucs>
</div>


<link rel="stylesheet" href="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/styles.css">
<script src="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/package-es5.js?v=<?= date_timestamp_get(date_create()); ?>" nomodule defer></script>
<script src="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/package-es2015.js?v=<?= date_timestamp_get(date_create()); ?>" type="module"></script>




<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Google Analytics -->
<script>
  $(window).load(function() {
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
          (
            i[r].q = i[r].q || []).push(arguments)
        },
        i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', '<?= $idAnalytics ?>', 'auto');
    ga('create', 'UA-47730935-51', 'auto', 'clientTracker');
    ga('send', 'pageview');
    ga('clientTracker.send', 'pageview');
    $LAB
      .script("js/geral.js").wait()
      .script("js/app.js").wait()
  });
</script>
<script>
  var url = window.location;
  $(function() {
    $('aside li a[href="' + url + '"]').addClass('active');
  });
</script>
<!-- Acompanhamento-->
<script>
  jQuery(document).ready(function($) {
    jQuery('.botao-cotar').on('click', function() {
      ga('send', 'event', 'Evento Orcamento', 'Clique', 'Clique Orcamento');
    });
  });
</script>
<script>
  jQuery(document).ready(function($) {
    jQuery('.linkwhats').on('click', function() {
      ga('send', 'event', 'Evento Whatsapp', 'Clique', 'Clique Whatsapp');
    });
  });
</script>
<!-- Fim Acompanhamento-->
<!-- Botão-->
<!-- <a style="position:fixed;bottom: 100px;right: 40px;z-index: 2000;cursor:pointer;outline:none!important;" class="linkwhats hide-desk" href="https://api.whatsapp.com/send?phone=5511948102289&text=Ol%C3%A1!%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20T.A.%20Refrigeração" target="_blank">
<img src="<?= $url ?>imagens/whatsapp.png" alt="Whatsapp Refrigeração Real"></a>
<a style="position:fixed;bottom: 100px;right: 40px;z-index: 2000;cursor:pointer;outline:none!important;" class="linkwhats hide-mobile" href="https://web.whatsapp.com/send?phone=5511948102289&text=Ol%C3%A1!%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20T.A.%20Refrigeração" target="_blank">
<img src="<?= $url ?>imagens/whatsapp.png" alt="Whatsapp Refrigeração Real"></a> -->
<!-- Botão-->
<!-- Tabs Regiões -->
<script defer src="<?= $url ?>js/organictabs.jquery.js"></script>
<!-- Script Launch start -->
<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
<script>
  const aside = document.querySelector('aside');
  const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
  aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
</script> <!-- Script Launch end -->