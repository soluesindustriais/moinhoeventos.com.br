<div class="wrapper-form">
	<br class="clear">
	<h2 class="blue">Faça sua cotação gratuitamente
	</h2>
	<div class="container contact">
		<?php
		$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		if (isset($post) && $post['enviar']):
		unset($post['enviar']);
		include("contato-envia.php");
		endif;
		?>
	</div>
	<div class="form-style-5">
		<form method="post" name="form" enctype="multipart/form-data">
			<fieldset>
				<div class="form-conteudo">
					<label for="nome">Nome:
						<span>*
						</span>
					</label>
					<input   type="text" name="nome" value="" id="nome"  required="">
					<label for="email">E-mail:
						<span>*
						</span>
					</label>
					<input  type="email" name="email" value="" id="email"  required="" >
					<label for="tel">Telefone:
						<span>*
						</span>
					</label>
					<input type="text" name="telefone" value="" id="tel" required="" maxlength="15">
					<label for="empresa">Empresa:
						<span>*
						</span>
					</label>
					<input type="text" name="empresa" value="" id="empresa" required="">
				</div>
				<div class="form-conteudo-1">
					<label for="produto">Produto:
						<span>*
						</span>
					</label>
					<input type="text" name="produto" value="" id="produto" required="">
					<label >Mensagem:
						<span>*
						</span>
					<textarea name="mensagem" rows="5" required=""></textarea>
					</label>
					<span class="bt-submit">
						<input type="submit" name="enviar" value="Enviar" class="ir">
					</span>
				</div>
			</fieldset>
			<span class="obrigatorio">Os campos com * são obrigatórios
			</span>
			<br class="clear">
		</form>
	</div>
</div>