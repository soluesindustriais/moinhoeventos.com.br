<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	
	<? include('inc/geral.php'); ?>
		<script src="<?=$url?>js/jquery-1.9.0.min.js"></script>
		<script src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
		<link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="stylesheet" href="css/normalize.css" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			
<!--SweetAlert Modal-->
		<link rel="stylesheet" href="<?=$url?>js/sweetalert/css/sweetalert.css" />
		<script src="<?=$url?>js/sweetalert/js/sweetalert.min.js"></script>
		<!--/SweetAlert Modal-->
		<!--Máscara dos campos-->
		<script src="<?=$url?>js/maskinput.js"></script>
		<script>
			$(function() {
				$('input[name="telefone"]').mask('(99) 99999-9999');
			});

		</script>

	<!-- MENU  MOBILE -->
	<script src="<?=$url?>js/jquery.slicknav.js"></script>
	<!-- /MENU  MOBILE -->
	<title><?=$title." - ".$nomeSite?></title>
	<base href="<?=$url?>">
	<meta name="description" content="<?=ucfirst($desc)?>">
	<meta name="keywords" content="<?=$h1.", ".$key?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="geo.position" content="<?=$latitude.";".$longitude?>">
	<meta name="geo.placename" content="<?=$cidade."-".$UF?>">
	<meta name="geo.region" content="<?=$UF?>-BR">
	<meta name="ICBM" content="<?=$latitude.";".$longitude?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?=$url.$urlPagina?>">
	<?php
	if ( $author == '')
	{
	echo '<meta name="author" content="'.$nomeSite.'">';
	}
	else
	{
	echo '<link rel="author" href="'.$author.'">';
	}
	?>
	
	<link rel="shortcut icon" href="<?=$url?>imagens/img-home/favicon.png">
	
	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?=$title." - ".$nomeSite?>">
	<meta property="og:type" content="article">
    <?php
        if (file_exists($url.$pasta.$urlPagina."-01.jpg")) {
     ?>
    <meta property="og:image" content="<?=$url.$pasta.$urlPagina?>-01.jpg">
    <?php
        }
     ?>
	<meta property="og:url" content="<?=$url.$urlPagina?>">
	<meta property="og:description" content="<?=$desc?>">
	<meta property="og:site_name" content="<?=$nomeSite?>">
	<meta property="fb:admins" content="<?=$idFacebook?>">
	
	<!-- Desenvolvido por <?=$creditos." - ".$siteCreditos?> -->
	<script>
	$(document).ready(function(){
	var altura = document.getElementById('scrollheader').offsetHeight;
	
	
	window.onscroll = function() {Scroll()};
	function Scroll() {
	if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
	document.getElementById("scrollheader").className = "topofixo";
	document.getElementById("header-block").style.display = "block";
	document.getElementById("header-block").style.height = altura+"px";
	
	} else {
	document.getElementById("scrollheader").className = "";
	document.getElementById("header-block").style.display = "none";
	}
	}
	});
	</script>