<?
$nomeSite			= 'Sistema de Incêndio';
$slogan				= 'Cotações de Sistemas de Incêndio com dezenas de empresas';
//$url				= 'http://www.solucoesindustriais.com.br/lista/site-base/';
$url				= 'http://localhost/sites-solucs/sistemasdeincendio.com.br/';
$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-ADICIONAR';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
</div>
';
$caminho2	= '
<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';
$caminhoBread 			= '
<div class="wrapper">
	<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
		<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
		<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
	</div>
	<h1>'.$h1.'</h1>
</div>
</div>
';
$caminhoBread2	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
		<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
<h1>'.$h1.'</h1>
</div>

';
$caminhocalibracao	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
		<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
			<a href="'.$url.'calibracao-categoria" title="Calibração" class="category" itemprop="url"><span itemprop="title"> Calibração </span></a> »
			<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';
$caminhocalibracao_de_balancas	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
		<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
			<a href="'.$url.'calibracao-de-balancas-categoria" title="Calibração de Balanças" class="category" itemprop="url"><span itemprop="title"> Calibração de Balanças </span></a> »
			<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';
?>