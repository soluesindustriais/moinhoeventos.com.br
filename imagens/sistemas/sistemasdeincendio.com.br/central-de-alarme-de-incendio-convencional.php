<? $h1 = "Central de alarme de incêndio convencional";
$title = "Central de alarme de incêndio convencional";
$desc = "Se procura por $h1, você só consegue nas buscas do Soluções Industriais, solicite uma cotação online com mais de 30 indústrias ao mesmo tempo";
$key = "Centrais de alarmes de incêndio convencional,Sistema de alarme de incêndio convencional";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>

<body>
    <? include('inc/topo.php'); ?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section>
                    <?= $caminhoinformacoes ?><br class="clear" />
                    <h1>
                        <?= $h1 ?>
                    </h1>
                    <article>
                        <div class="img-mpi"><a
                                href="<?= $url ?>imagens/mpi/central-de-alarme-de-incendio-convencional-01.jpg"
                                title="<?= $h1 ?>" class="lightbox"><img
                                    src="<?= $url ?>imagens/mpi/thumbs/central-de-alarme-de-incendio-convencional-01.jpg"
                                    title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a
                                href="<?= $url ?>imagens/mpi/central-de-alarme-de-incendio-convencional-02.jpg"
                                title="Centrais de alarmes de incêndio convencional" class="lightbox"><img
                                    src="<?= $url ?>imagens/mpi/thumbs/central-de-alarme-de-incendio-convencional-02.jpg"
                                    title="Centrais de alarmes de incêndio convencional"
                                    alt="Centrais de alarmes de incêndio convencional"></a><a
                                href="<?= $url ?>imagens/mpi/central-de-alarme-de-incendio-convencional-03.jpg"
                                title="Sistema de alarme de incêndio convencional" class="lightbox"><img
                                    src="<?= $url ?>imagens/mpi/thumbs/central-de-alarme-de-incendio-convencional-03.jpg"
                                    title="Sistema de alarme de incêndio convencional"
                                    alt="Sistema de alarme de incêndio convencional"></a></div><span class="aviso">Estas
                            imagens foram obtidas de bancos de imagens públicas e disponível livremente na
                            internet</span>
                        <hr />
                        <p>indicados para pequenas e médias instalações. A sensibilidade de seus detectores de fumaça ou
                            temperatura já vem pré-ajustada de fábrica, ou seja, não pode ser alterada.</p>
                        <h2>Características dos sistemas de detecção e <strong>Central de alarme de incêndio
                                convencional</strong></h2>
                        <p>Nesses sistemas, a ativação de um dispositivo mostra quando há um foco de incêndio em alguma
                            região da área que é protegida. Por isso, é importante conhecer alguns componentes dos
                            sistemas de detecção convencional que são encontrados no mercado:</p>
                        <ul>
                            <li class="li-mpi">Painel Convencional de Incêndio</li>
                            <li class="li-mpi">Detector de Fumaça Óptico Convencional</li>
                            <li class="li-mpi">Cabo de Sinal de Detecção.</li>
                        </ul>
                        <p>O <strong>Central de alarme de incêndio convencional</strong> é de fácil instalação e para
                            garantir um equipamento confiável e seguro é necessário contar com uma empresa de confiança,
                            pois junto com o alarme é instalada a central convencional e até 20 dispositivos, que são
                            detectores de fumaça ou temperatura.</p>
                        <h2>Sistema de detecção e <strong>Central de alarme de incêndio convencional</strong> de
                            excelência</h2>
                        <p>Entre em contato, estamos a disposição para consultas sobre elaboração e aprovação de
                            projetos de prevenção e combate a incêndio laudos e atestados formação e manutenção de
                            brigadas e muitos outros serviços de alta qualidade.</p>
                    </article>
                    <? include('inc/coluna-mpi.php'); ?><br class="clear">
                    <? include('inc/busca-mpi.php'); ?>
                    <? include('inc/form-mpi.php'); ?>
                    <? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div>
    <? include('inc/footer.php'); ?>
</body>

</html>