<h2 class="h2-regioes" >Principais cidades e regiões do Brasil onde a <?=$nomeSite?> atende:</h2><br>
<div id="servicosTabsDois">
      <div>
            <ul class="nav">
                  
                  <li class="nav-two"><a href="#selecione2" class="current" title="Selecione">Selecione</a></li>
                  
                  <li class="nav-two"><a href="#rj" title="RJ - Rio de Janeiro">RJ</a></li>
                  <li class="nav-two"><a href="#mg" title="MG - Minas Gerais">MG</a></li>
                  <li class="nav-two"><a href="#es" title="ES - Espírito Santo">ES</a></li>
                  <li class="nav-two"><a href="#sp" title="SP - Litoral e Interior">SP</a></li>
                  
                  <li class="nav-two"><a href="#pr" title="PR - Paraná">PR</a></li>
                  <li class="nav-two"><a href="#sc" title="SC - Santa Catarina">SC</a></li>
                  <li class="nav-two"><a href="#rs" title="RS - Rio Grande do Sul">RS</a></li>
                  
                  <li class="nav-two"><a href="#pe" title="PE - Pernambuco">PE</a></li>
                  <li class="nav-two"><a href="#ba" title="BA - Bahia">BA</a></li>
                  <li class="nav-two"><a href="#ce" title="CE - Ceará">CE</a></li>
                  
                  <li class="nav-two"><a href="#go" title="GO e DF - Goiás e Distrito Federal">GO e DF</a></li>
                  
                  <li class="nav-two"><a href="#am" title="AM - Amazonas">AM</a></li>
                  <li class="nav-two"><a href="#pa" title="PA - Pará">PA</a></li>
                  <li class="nav-two"><a href="#ac" title="AC - Acre">AC</a></li>
                  <li class="nav-two"><a href="#al" title="AL - Alagoas">AL</a></li>
                  <li class="nav-two"><a href="#ap" title="AP - Amapá">AP</a></li>
                  <li class="nav-two"><a href="#ma" title="MA - Maranhão">MA</a></li>
                  <li class="nav-two"><a href="#mt" title="MT - Mato Grosso">MT</a></li>
                  <li class="nav-two"><a href="#ms" title="MS - Mato Grosso do Sul">MS</a></li>
                  <li class="nav-two"><a href="#pb" title="PB - Paraíba">PB</a></li>
                  <li class="nav-two"><a href="#pi" title="PI - Piauí">PI</a></li>
                  <li class="nav-two"><a href="#rn" title="RN - Rio Grande do Norte">RN</a></li>
                  <li class="nav-two"><a href="#ro" title="RO - Rondônia">RO</a></li>
                  <li class="nav-two"><a href="#rr" title="RR - Roraima">RR</a></li>
                  <li class="nav-two"><a href="#se" title="SE - Sergipe">SE</a></li>
                  <li class="nav-two"><a href="#to" title="TO - Tocantins">TO</a></li>
            </ul>
      </div>
      <div class="list-wrap">
            
            <ul id="selecione2">
                  <li><strong>Selecione a região do Brasil</strong></li>
            </ul>
            <ul id="rj" class="hide">
                  <li><strong>Rio de Janeiro </strong></li>
                  <li><strong>São Gonçalo</strong></li>
                  <li><strong>Duque de Caxias</strong></li>
                  <li><strong>Nova Iguaçu</strong></li>
                  <li><strong>Niterói</strong></li>
                  <li><strong>Belford Roxo</strong></li>
                  <li><strong>São João de Meriti</strong></li>
                  <li><strong>Campos dos Goytacazes</strong></li>
                  <li><strong>Petrópolis</strong></li>
                  <li><strong>Volta Redonda</strong></li>
                  <li><strong>Magé</strong></li>
                  <li><strong>Itaboraí</strong></li>
                  <li><strong>Mesquita</strong></li>
                  <li><strong>Nova Friburgo</strong></li>
                  <li><strong>Barra Mansa</strong></li>
                  <li><strong>Macaé</strong></li>
                  <li><strong>Cabo Frio</strong></li>
                  <li><strong>Nilópolis</strong></li>
                  <li><strong>Teresópolis</strong></li>
                  <li><strong>Resende</strong></li>
            </ul>
            
            <ul id="mg" class="hide">
                  <li><strong>Belo Horizonte</strong></li>
                  <li><strong>Uberlândia</strong></li>
                  <li><strong>Contagem</strong></li>
                  <li><strong>Juiz de Fora</strong></li>
                  <li><strong>Betim</strong></li>
                  <li><strong>Montes Claros</strong></li>
                  <li><strong>Ribeirão das Neves</strong></li>
                  <li><strong>Uberaba</strong></li>
                  <li><strong>Governador Valadares</strong></li>
                  <li><strong>Ipatinga</strong></li>
                  <li><strong>Santa Luzia</strong></li>
                  <li><strong>Sete Lagoas</strong></li>
                  <li><strong>Divinópolis</strong></li>
                  <li><strong>Ibirité</strong></li>
                  <li><strong>Poços de Caldas</strong></li>
                  <li><strong>Patos de Minas</strong></li>
                  <li><strong>Teófilo Otoni</strong></li>
                  <li><strong>Sabará</strong></li>
                  <li><strong>Pouso Alegre</strong></li>
                  <li><strong>Barbacena</strong></li>
                  <li><strong>Varginha</strong></li>
                  <li><strong>Conselheiro Lafeiete</strong></li>
                  <li><strong>Araguari</strong></li>
                  <li><strong>Itabira</strong></li>
                  <li><strong>Passos</strong></li>
            </ul>
            
            <ul id="es" class="hide">
                  <li><strong>Serra</strong></li>
                  <li><strong>Vila Velha</strong></li>
                  <li><strong>Cariacica</strong></li>
                  <li><strong>Vitória</strong></li>
                  <li><strong>Cachoeiro de Itapemirim</strong></li>
                  <li><strong>Linhares</strong></li>
                  <li><strong>São Mateus</strong></li>
                  <li><strong>Colatina</strong></li>
                  <li><strong>Guarapari</strong></li>
                  <li><strong>Aracruz</strong></li>
                  <li><strong>Viana</strong></li>
                  <li><strong>Nova Venécia</strong></li>
                  <li><strong>Barra de São Francisco</strong></li>
                  <li><strong>Santa Maria de Jetibá</strong></li>
                  <li><strong>Castelo</strong></li>
                  <li><strong>Marataízes</strong></li>
                  <li><strong>São Gabriel da Palha</strong></li>
                  <li><strong>Domingos Martins</strong></li>
                  <li><strong>Itapemirim</strong></li>
                  <li><strong>Afonso Cláudio</strong></li>
                  <li><strong>Alegre</strong></li>
                  <li><strong>Baixo Guandu</strong></li>
                  <li><strong>Conceição da Barra</strong></li>
                  <li><strong>Guaçuí</strong></li>
                  <li><strong>Iúna</strong></li>
                  <li><strong>Jaguaré</strong></li>
                  <li><strong>Mimoso do Sul</strong></li>
                  <li><strong>Sooretama</strong></li>
                  <li><strong>Anchieta</strong></li>
                  <li><strong>Pinheiros</strong></li>
                  <li><strong>Pedro Canário</strong></li>
            </ul>
            
            <ul id="sp" class="hide">
                  <li><strong>Bertioga</strong></li>
                  <li><strong>Caraguatatuba</strong></li>
                  <li><strong>Cubatão</strong></li>
                  <li><strong>Guarujá</strong></li>
                  <li><strong>Ilhabela</strong></li>
                  <li><strong>Itanhaém</strong></li>
                  <li><strong>Mongaguá</strong></li>
                  <li><strong>Riviera de São Lourenço</strong></li>
                  <li><strong>Santos</strong></li>
                  <li><strong>São Vicente</strong></li>
                  <li><strong>Praia Grande</strong></li>
                  <li><strong>Ubatuba</strong></li>
                  <li><strong>São Sebastião</strong></li>
                  <li><strong>Peruíbe</strong></li>
                  <li><strong>São José dos campos</strong></li>
                  <li><strong>Campinas</strong></li>
                  <li><strong>Jundiaí</strong></li>
                  <li><strong>Sorocaba</strong></li>
                  <li><strong>Indaiatuba </strong></li>
                  <li><strong>São José do Rio Preto </strong></li>
                  <li><strong>Itatiba </strong></li>
                  <li><strong>Amparo </strong></li>
                  <li><strong>Barueri </strong></li>
                  <li><strong>Ribeirão Preto</strong></li>
                  <li><strong>Marília </strong></li>
                  <li><strong>Louveira </strong></li>
                  <li><strong>Paulínia </strong></li>
                  <li><strong>Bauru </strong></li>
                  <li><strong>Valinhos </strong></li>
                  <li><strong>Bragança Paulista </strong></li>
                  <li><strong>Araraquara</strong></li>
                  <li><strong>Americana</strong></li>
                  <li><strong>Atibaia </strong></li>
                  <li><strong>Taubaté </strong></li>
                  <li><strong>Araras </strong></li>
                  <li><strong>São Carlos </strong></li>
                  <li><strong>Itupeva </strong></li>
                  <li><strong>Mendonça </strong></li>
                  <li><strong>Itu </strong></li>
                  <li><strong>Vinhedo </strong></li>
                  <li><strong>Marapoama </strong></li>
                  <li><strong>Votuporanga </strong></li>
                  <li><strong>Hortolândia </strong></li>
                  <li><strong>Araçatuba </strong></li>
                  <li><strong>Jaboticabal </strong></li>
                  <li><strong>Sertãozinho</strong></li>
            </ul>
            
            
            <ul id="pr" class="hide">
                  <li><strong>Curitiba</strong></li>
                  <li><strong>Londrina</strong></li>
                  <li><strong>Maringá</strong></li>
                  <li><strong>Ponta Grossa</strong></li>
                  <li><strong>Cascavel</strong></li>
                  <li><strong>São José dos Pinhais</strong></li>
                  <li><strong>Foz do Iguaçu</strong></li>
                  <li><strong>Colombo</strong></li>
                  <li><strong>Guarapuava</strong></li>
                  <li><strong>Paranaguá</strong></li>
                  <li><strong>Araucária</strong></li>
                  <li><strong>Toledo</strong></li>
                  <li><strong>Apucarana</strong></li>
                  <li><strong>Pinhais</strong></li>
                  <li><strong>Campo Largo</strong></li>
                  <li><strong>Almirante Tamandaré</strong></li>
                  <li><strong>Umuarama</strong></li>
                  <li><strong>Paranavaí</strong></li>
                  <li><strong>Piraquara</strong></li>
                  <li><strong>Cambé</strong></li>
                  <li><strong>Sarandi</strong></li>
                  <li><strong>Fazenda Rio Grande</strong></li>
                  <li><strong>Paranavaí</strong></li>
                  <li><strong>Francisco Beltrão</strong></li>
                  <li><strong>Pato Branco</strong></li>
                  <li><strong>Cianorte</strong></li>
                  <li><strong>Telêmaco Borba</strong></li>
                  <li><strong>Castro</strong></li>
                  <li><strong>Rolândia</strong></li>
            </ul>
            
            <ul id="sc" class="hide">
                  <li><strong>Joinville</strong></li>
                  <li><strong>Florianópolis</strong></li>
                  <li><strong>Blumenau</strong></li>
                  <li><strong>Itajaí</strong></li>
                  <li><strong>São José</strong></li>
                  <li><strong>Chapecó</strong></li>
                  <li><strong>Criciúma</strong></li>
                  <li><strong>Jaraguá do sul</strong></li>
                  <li><strong>Lages</strong></li>
                  <li><strong>Palhoça</strong></li>
                  <li><strong>Balneário Camboriú</strong></li>
                  <li><strong>Brusque</strong></li>
                  <li><strong>Tubarão</strong></li>
                  <li><strong>São Bento do Sul</strong></li>
                  <li><strong>Caçador</strong></li>
                  <li><strong>Concórdia</strong></li>
                  <li><strong>Camboriú</strong></li>
                  <li><strong>Navegantes</strong></li>
                  <li><strong>Rio do Sul</strong></li>
                  <li><strong>Araranguá</strong></li>
                  <li><strong>Gaspar</strong></li>
                  <li><strong>Biguaçu</strong></li>
                  <li><strong>Indaial</strong></li>
                  <li><strong>Mafra</strong></li>
                  <li><strong>Canoinhas</strong></li>
                  <li><strong>Itapema</strong></li>
            </ul>
            
            <ul id="rs" class="hide">
                  <li><strong>Porto Alegre</strong></li>
                  <li><strong>Caxias do Sul</strong></li>
                  <li><strong>Pelotas</strong></li>
                  <li><strong>Canoas</strong></li>
                  <li><strong>Santa Maria</strong></li>
                  <li><strong>Gravataí</strong></li>
                  <li><strong>Viamão</strong></li>
                  <li><strong>Novo Hamburgo</strong></li>
                  <li><strong>São Leopoldo</strong></li>
                  <li><strong>Rio Grande</strong></li>
                  <li><strong>Alvorada</strong></li>
                  <li><strong>Passo Fundo</strong></li>
                  <li><strong>Sapucaia do Sul</strong></li>
                  <li><strong>Uruguaiana</strong></li>
                  <li><strong>Santa Cruz do Sul</strong></li>
                  <li><strong>Cachoeirinha</strong></li>
                  <li><strong>Bagé</strong></li>
                  <li><strong>Bento Gonçalves</strong></li>
                  <li><strong>Erechim</strong></li>
                  <li><strong>Guaíba</strong></li>
                  <li><strong>Cachoeira do Sul</strong></li>
                  <li><strong>Santana do Livramento</strong></li>
                  <li><strong>Esteio</strong></li>
                  <li><strong>Ijuí</strong></li>
                  <li><strong>Alegrete</strong></li>
            </ul>
            
            
            <ul id="pe" class="hide">
                  <li><strong>Recife</strong></li>
                  <li><strong>Jaboatão dos Guararapes</strong></li>
                  <li><strong>Olinda</strong></li>
                  <li><strong>Caruaru</strong></li>
                  <li><strong>Petrolina</strong></li>
                  <li><strong>Paulista</strong></li>
                  <li><strong>Cabo de Santo Agostinho</strong></li>
                  <li><strong>Camaragibe</strong></li>
                  <li><strong>Garanhuns</strong></li>
                  <li><strong>Vitória de Santo Antão</strong></li>
                  <li><strong>Igarassu</strong></li>
                  <li><strong>São Lourenço da Mata</strong></li>
                  <li><strong>Abreu e Lima</strong></li>
                  <li><strong>Santa Cruz do Capibaribe</strong></li>
                  <li><strong>Ipojuca</strong></li>
                  <li><strong>Serra Talhada</strong></li>
                  <li><strong>Araripina</strong></li>
                  <li><strong>Gravatá</strong></li>
                  <li><strong>Carpina</strong></li>
                  <li><strong>Goiana</strong></li>
                  <li><strong>Belo Jardim</strong></li>
                  <li><strong>Arcoverde</strong></li>
                  <li><strong>Ouricuri</strong></li>
                  <li><strong>Escada</strong></li>
                  <li><strong>Pesqueira</strong></li>
                  <li><strong>Surubim</strong></li>
                  <li><strong>Palmares</strong></li>
                  <li><strong>Bezerros</strong></li>
            </ul>
            
            <ul id="ba" class="hide">
                  <li><strong>Salvador</strong></li>
                  <li><strong>Feira de Santana</strong></li>
                  <li><strong>Vitória da Conquista</strong></li>
                  <li><strong>Camaçari</strong></li>
                  <li><strong>Itabuna</strong></li>
                  <li><strong>Juazeiro</strong></li>
                  <li><strong>Lauro de Freitas</strong></li>
                  <li><strong>Ilhéus</strong></li>
                  <li><strong>Jequié</strong></li>
                  <li><strong>Teixeira de Freitas</strong></li>
                  <li><strong>Alagoinhas</strong></li>
                  <li><strong>Barreiras</strong></li>
                  <li><strong>Porto Seguro</strong></li>
                  <li><strong>Simões Filho</strong></li>
                  <li><strong>Paulo Afonso</strong></li>
                  <li><strong>Eunápolis</strong></li>
                  <li><strong>Santo Antônio de Jesus</strong></li>
                  <li><strong>Valença</strong></li>
                  <li><strong>Candeias</strong></li>
                  <li><strong>Guanambi</strong></li>
                  <li><strong>Jacobina</strong></li>
                  <li><strong>Serrinha</strong></li>
                  <li><strong>Senhor do Bonfim</strong></li>
                  <li><strong>Dias d'Ávila</strong></li>
                  <li><strong>Luís Eduardo Magalhães</strong></li>
                  <li><strong>Itapetinga</strong></li>
                  <li><strong>Irecê</strong></li>
                  <li><strong>Campo Formoso</strong></li>
                  <li><strong>Casa Nova</strong></li>
                  <li><strong>Brumado</strong></li>
                  <li><strong>Bom Jesus da Lapa</strong></li>
                  <li><strong>Conceição do Coité</strong></li>
                  <li><strong>Itamaraju</strong></li>
                  <li><strong>Itaberaba</strong></li>
                  <li><strong>Cruz das Almas</strong></li>
                  <li><strong>Ipirá</strong></li>
                  <li><strong>Santo Amaro</strong></li>
                  <li><strong>Euclides da Cunha</strong></li>
            </ul>
            
            <ul id="ce" class="hide">
                  <li><strong>Fortaleza</strong></li>
                  <li><strong>caucacia</strong></li>
                  <li><strong>Juazeiro do Norte</strong></li>
                  <li><strong>Maracanaú</strong></li>
                  <li><strong>Sobral</strong></li>
                  <li><strong>Crato</strong></li>
                  <li><strong>Itapipoca</strong></li>
                  <li><strong>Maranguape</strong></li>
                  <li><strong>Iguatu</strong></li>
                  <li><strong>Quixadá</strong></li>
                  <li><strong>Canindé</strong></li>
                  <li><strong>Pacajus</strong></li>
                  <li><strong>Crateús</strong></li>
                  <li><strong>Aquiraz</strong></li>
                  <li><strong>Pacatuba</strong></li>
                  <li><strong>Quixeramobim</strong></li>
            </ul>
            
            <ul id="go" class="hide">
                  <li><strong>Goiânia</strong></li>
                  <li><strong>Aparecida de Goiânia</strong></li>
                  <li><strong>Anápolis</strong></li>
                  <li><strong>Rio Verde</strong></li>
                  <li><strong>Luziânia</strong></li>
                  <li><strong>Águas Lindas de Goiás</strong></li>
                  <li><strong>Valparaíso de Goiás</strong></li>
                  <li><strong>Trindade</strong></li>
                  <li><strong>Formosa</strong></li>
                  <li><strong>Novo Gama</strong></li>
                  <li><strong>Itumbiara</strong></li>
                  <li><strong>Senador Canedo</strong></li>
                  <li><strong>Catalão</strong></li>
                  <li><strong>Jataí</strong></li>
                  <li><strong>Planaltina</strong></li>
                  <li><strong>Caldas Novas</strong></li>
            </ul>
            
            
            <ul id="am" class="hide">
                  <li><strong>Manaus</strong></li>
                  <li><strong>Parintins</strong></li>
                  <li><strong>Itacoatiara</strong></li>
                  <li><strong>Manacapuru</strong></li>
                  <li><strong>Coari</strong></li>
                  <li><strong>Centro Amazonense</strong></li>
            </ul>
            
            <ul id="pa" class="hide">
                  <li><strong>Belém</strong></li>
                  <li><strong>Ananindeua</strong></li>
                  <li><strong>Santarém</strong></li>
                  <li><strong>Marabá</strong></li>
                  <li><strong>Castanhal</strong></li>
                  <li><strong>Parauapebas</strong></li>
                  <li><strong>Itaituba</strong></li>
                  <li><strong>Cametá</strong></li>
                  <li><strong>Bragança </strong></li>
                  <li><strong>Abaetetuba</strong></li>
                  <li><strong>Bragança</strong></li>
                  <li><strong>Marituba</strong></li>
            </ul>
            <ul id="ac" class="hide">
                  <li><strong>Acrelândia</strong></li>
                  <li><strong>Assis Brasil</strong></li>
                  <li><strong>Brasiléia</strong></li>
                  <li><strong>Bujari</strong></li>
                  <li><strong>Capixaba</strong></li>
                  <li><strong>Cruzeiro do Sul</strong></li>
                  <li><strong>Epitaciolândia</strong></li>
                  <li><strong>Feijó</strong></li>
                  <li><strong>Jordão</strong></li>
                  <li><strong>Mâncio Lima</strong></li>
                  <li><strong>Manoel Urbano (Manuel Urbano)</strong></li>
                  <li><strong>Marechal Thaumaturgo (Taumaturgo, Thaumaturgo)</strong></li>
                  <li><strong>Plácido de Castro</strong></li>
                  <li><strong>Porto Acre</strong></li>
                  <li><strong>Porto Walter</strong></li>
                  <li><strong>Rio Branco*</strong></li>
                  <li><strong>Rodrigues Alves</strong></li>
                  <li><strong>Santa Rosa do Purus</strong></li>
                  <li><strong>Sena Madureira</strong></li>
                  <li><strong>Senador Guiomard</strong></li>
                  <li><strong>Tarauacá</strong></li>
                  <li><strong>Xapuri</strong></li>
            </ul>
            <ul id="al" class="hide">
                  <li><strong>Maceió</strong></li>
                  <li><strong>Maragogi</strong></li>
                  <li><strong>Atalaia</strong></li>
                  <li><strong>Batalha</strong></li>
                  <li><strong>Delmiro Gouveia</strong></li>
                  <li><strong>Mata Grande</strong></li>
                  <li><strong>Palmeira dos Índios</strong></li>
                  <li><strong>Arapiraca</strong></li>
                  <li><strong>Coruripe</strong></li>
                  <li><strong>São Miguel dos Campos</strong></li>
                  <li><strong>Santana do Ipanema</strong></li>
            </ul>
            <ul id="ap" class="hide">
                  <li><strong>Macapá</strong></li>
                  <li><strong>Santana (Porto Santana)</strong></li>
                  <li><strong>Laranjal do Jari</strong></li>
                  <li><strong>Oiapoque</strong></li>
                  <li><strong>Porto Grande</strong></li>
                  <li><strong>Vitória do Jari</strong></li>
                  <li><strong>Mazagão</strong></li>
                  <li><strong>Calçoene</strong></li>
                  <li><strong>Pedra Branca do Amapari (Pedra Branca do Amaparí)</strong></li>
                  <li><strong>Tartarugalzinho</strong></li>
                  <li><strong>Amapá</strong></li>
                  <li><strong>Ferreira Gomes</strong></li>
                  <li><strong>Serra do Navio</strong></li>
                  <li><strong>Cutias</strong></li>
                  <li><strong>Pracuúba</strong></li>
                  <li><strong>Itaubal</strong></li>
            </ul>
            <ul id="ma" class="hide">
                  <li><strong>São Luís</strong></li>
                  <li><strong>Imperatriz</strong></li>
                  <li><strong>São José de Ribamar</strong></li>
                  <li><strong>Timon</strong></li>
                  <li><strong>Caixas</strong></li>
                  <li><strong>Codó</strong></li>
                  <li><strong>Paço do Lumiar</strong></li>
                  <li><strong>Açailândia</strong></li>
                  <li><strong>Bacabal</strong></li>
                  <li><strong>Balsas</strong></li>
                  <li><strong>Barra do Corda</strong></li>
                  <li><strong>Pinheiro</strong></li>
                  <li><strong>Santa Inês</strong></li>
                  <li><strong>Santa Luzia</strong></li>
                  <li><strong>Chapadinha</strong></li>
                  <li><strong>Buriticupú</strong></li>
                  <li><strong>Grajaú</strong></li>
                  <li><strong>Coroatá</strong></li>
                  <li><strong>Itapecuru-Mirim</strong></li>
                  <li><strong>Barreirinhas</strong></li>
            </ul>
            <ul id="mt" class="hide">
                  <li><strong>Cuiabá</strong></li>
                  <li><strong>Várzea Grande</strong></li>
                  <li><strong>Rondonópolis</strong></li>
                  <li><strong>Sinop</strong></li>
                  <li><strong>Tangará da Serra</strong></li>
                  <li><strong>Cáceres</strong></li>
                  <li><strong>Sorriso</strong></li>
                  <li><strong>Lucas do Rio Verde</strong></li>
                  <li><strong>Barra do Garças</strong></li>
                  <li><strong>Primavera do Leste</strong></li>
                  <li><strong>Alta Floresta</strong></li>
                  <li><strong>Pontes e Lacerda</strong></li>
                  <li><strong>Nova Mutum</strong></li>
                  <li><strong>Campo Verde</strong></li>
                  <li><strong>Juína</strong></li>
                  <li><strong>Colniza</strong></li>
                  <li><strong>Guarantã do Norte</strong></li>
                  <li><strong>Juara</strong></li>
                  <li><strong>Barra do Bugres</strong></li>
                  <li><strong>Peixoto de Azevedo</strong></li>
            </ul>
            <ul id="ms" class="hide">
                  <li><strong>Campo Grande</strong></li>
                  <li><strong>Dourados</strong></li>
                  <li><strong>Três Lagoas</strong></li>
                  <li><strong>Corumbá</strong></li>
                  <li><strong>Ponta Porã</strong></li>
                  <li><strong>Naviraí</strong></li>
                  <li><strong>Nova Andradina</strong></li>
                  <li><strong>Aquidauana</strong></li>
                  <li><strong>Sidrolândia</strong></li>
                  <li><strong>Paranaíba</strong></li>
                  <li><strong>Maracaju</strong></li>
                  <li><strong>Amambai</strong></li>
                  <li><strong>Coxim</strong></li>
                  <li><strong>Rio Brilhante</strong></li>
                  <li><strong>Caarapó</strong></li>
            </ul>
            <ul id="pb" class="hide">
                  <li><strong>João Pessoa</strong></li>
                  <li><strong>Campina Grande</strong></li>
                  <li><strong>Santa Rita</strong></li>
                  <li><strong>Patos</strong></li>
                  <li><strong>Bayeux</strong></li>
                  <li><strong>Sousa</strong></li>
                  <li><strong>Cabedelo</strong></li>
                  <li><strong>Cajazeiras</strong></li>
                  <li><strong>Guarabira</strong></li>
                  <li><strong>Sapé</strong></li>
                  <li><strong>Mamanguape</strong></li>
                  <li><strong>Queimadas</strong></li>
                  <li><strong>São Bento</strong></li>
                  <li><strong>Monteiro</strong></li>
                  <li><strong>Esperança</strong></li>
                  <li><strong>Pombal</strong></li>
                  <li><strong>Catolé do Rocha</strong></li>
                  <li><strong>Alagoa Grande</strong></li>
                  <li><strong>Pedras de Fogo</strong></li>
                  <li><strong>Lagoa Seca</strong></li>
            </ul>
            <ul id="pi" class="hide">
                  <li><strong>Teresina</strong></li>
                  <li><strong>Parnaíba</strong></li>
                  <li><strong>Picos</strong></li>
                  <li><strong>Piripiri</strong></li>
                  <li><strong>Floriano</strong></li>
                  <li><strong>Barras</strong></li>
                  <li><strong>Campo Maior</strong></li>
                  <li><strong>União</strong></li>
                  <li><strong>Altos</strong></li>
                  <li><strong>Esperantina</strong></li>
                  <li><strong>José de Freitas</strong></li>
                  <li><strong>Pedro II</strong></li>
                  <li><strong>Oeiras</strong></li>
                  <li><strong>São Raimundo Nonato</strong></li>
                  <li><strong>Miguel Alves</strong></li>
                  <li><strong>Luís Correia</strong></li>
                  <li><strong>Piracuruca</strong></li>
                  <li><strong>Cocal</strong></li>
                  <li><strong>Batalha</strong></li>
                  <li><strong>Corrente</strong></li>
            </ul>
            <ul id="rn" class="hide">
                  <li><strong>Natal</strong></li>
                  <li><strong>Mossoró</strong></li>
                  <li><strong>Parnamirim</strong></li>
                  <li><strong>São Gonçalo do Amarante</strong></li>
                  <li><strong>Macaíba</strong></li>
                  <li><strong>Ceará-Mirim</strong></li>
                  <li><strong>Caicó</strong></li>
                  <li><strong>Açu</strong></li>
                  <li><strong>Currais Novos</strong></li>
                  <li><strong>São José de Mipibu</strong></li>
                  <li><strong>Santa Cruz</strong></li>
                  <li><strong>Nova Cruz</strong></li>
                  <li><strong>Apodi</strong></li>
                  <li><strong>João Câmara</strong></li>
                  <li><strong>Canguaretama</strong></li>
                  <li><strong>Touros</strong></li>
                  <li><strong>Macau</strong></li>
                  <li><strong>Pau dos Ferros</strong></li>
                  <li><strong>Extremoz</strong></li>
                  <li><strong>Baraúna</strong></li>
            </ul>
            <ul id="ro" class="hide">
                  <li><strong>Porto Velho</strong></li>
                  <li><strong>Ji-Paraná</strong></li>
                  <li><strong>Ariquemes</strong></li>
                  <li><strong>Vilhena</strong></li>
                  <li><strong>Cacoal</strong></li>
                  <li><strong>Rolim de Moura</strong></li>
                  <li><strong>Jaru</strong></li>
                  <li><strong>Guajará-Mirim</strong></li>
                  <li><strong>Machadinho d'Oeste</strong></li>
                  <li><strong>Buritis</strong></li>
                  <li><strong>Pimenta Bueno</strong></li>
                  <li><strong>Ouro Preto do Oeste</strong></li>
                  <li><strong>Espigão d'Oeste</strong></li>
                  <li><strong>Nova Mamoré</strong></li>
                  <li><strong>Candeias do Jamari</strong></li>
                  <li><strong>Cujubim</strong></li>
                  <li><strong>Alta Floresta d'Oeste</strong></li>
                  <li><strong>São Miguel do Guaporé</strong></li>
                  <li><strong>Alto Paraíso</strong></li>
                  <li><strong>Nova Brasilândia d'Oeste</strong></li>
            </ul>
            <ul id="rr" class="hide">
                  <li><strong>Boa Vista</strong></li>
                  <li><strong>Rorainópolis</strong></li>
                  <li><strong>Caracaraí</strong></li>
                  <li><strong>Cantá</strong></li>
                  <li><strong>Mucajaí</strong></li>
                  <li><strong>Alto Alegre</strong></li>
                  <li><strong>Pacaraima</strong></li>
                  <li><strong>Amajari</strong></li>
                  <li><strong>Bonfim</strong></li>
                  <li><strong>Iracema</strong></li>
                  <li><strong>Normandia</strong></li>
                  <li><strong>Uiramutã</strong></li>
                  <li><strong>Caroebe</strong></li>
                  <li><strong>São João da Baliza</strong></li>
                  <li><strong>São Luís</strong></li>
                  <li><strong>Candeias do Jamari</strong></li>
                  <li><strong>Cujubim</strong></li>
                  <li><strong>Alta Floresta d'Oeste</strong></li>
                  <li><strong>São Miguel do Guaporé</strong></li>
                  <li><strong>Alto Paraíso</strong></li>
                  <li><strong>Nova Brasilândia d'Oeste</strong></li>
            </ul>
            <ul id="se" class="hide">
                  <li><strong>Aracaju</strong></li>
                  <li><strong>Nossa Senhora do Socorro</strong></li>
                  <li><strong>Lagarto</strong></li>
                  <li><strong>Itabaiana</strong></li>
                  <li><strong>São Cristóvão</strong></li>
                  <li><strong>Estância</strong></li>
                  <li><strong>Tobias Barreto</strong></li>
                  <li><strong>Itabaianinha</strong></li>
                  <li><strong>Simão Dias</strong></li>
                  <li><strong>Nossa Senhora da Glória</strong></li>
                  <li><strong>Poço Redondo</strong></li>
                  <li><strong>Itaporanga d'Ajuda</strong></li>
                  <li><strong>Capela</strong></li>
                  <li><strong>Barra dos Coqueiros</strong></li>
                  <li><strong>Laranjeiras</strong></li>
                  <li><strong>Propriá</strong></li>
                  <li><strong>Canindé de São Francisco</strong></li>
                  <li><strong>Porto da Folha</strong></li>
                  <li><strong>Boquim</strong></li>
                  <li><strong>Nossa Senhora das Dores</strong></li>
            </ul>
            <ul id="to" class="hide">
                  <li><strong>Palmas</strong></li>
                  <li><strong>Araguaína</strong></li>
                  <li><strong>Gurupi</strong></li>
                  <li><strong>Porto Nacional</strong></li>
                  <li><strong>Paraíso do Tocantins</strong></li>
                  <li><strong>Araguatins</strong></li>
                  <li><strong>Colinas do Tocantins</strong></li>
                  <li><strong>Guaraí</strong></li>
                  <li><strong>Tocantinópolis</strong></li>
                  <li><strong>Dianópolis</strong></li>
                  <li><strong>Miracema do Tocantins</strong></li>
                  <li><strong>Formoso do Araguaia</strong></li>
                  <li><strong>Augustinópolis</strong></li>
                  <li><strong>Taguatinga</strong></li>
                  <li><strong>Pedro Afonso</strong></li>
                  <li><strong>Miranorte</strong></li>
                  <li><strong>Lagoa da Confusão</strong></li>
                  <li><strong>Goiatins</strong></li>
                  <li><strong>São Miguel do Tocantins</strong></li>
                  <li><strong>Nova Olinda</strong></li>
                  <li><strong>Nossa Senhora das Dores</strong></li>
            </ul>
      </div>
</div>