<div class="container">
    <div class="wrapper">
        <h2 class="title-underline fs-28 text-center">Produtos em destaque</h2>
        <ul class="thumbnails-main grid-col-4">
            <?php
            $vetPalavras = [
                "exemplo-mpi",
                "duda-godoi",
                "kamila-fonseca",
                "exemplo-mpi-2"
            ];

            foreach ($vetPalavras as $itemDestaque) {
                $itemDestaqueSemAcento = remove_acentos($itemDestaque);
                $itemDestaqueComEspacos = str_replace('-', ' ', $itemDestaque);

                echo "<li>
						<a href=\"{$url}{$itemDestaqueSemAcento}\">
							<div class=\"overflow-hidden\">
								<img class=\"lazyload\" data-src=\"{$url}imagens/thumbs/{$itemDestaqueSemAcento}-4.webp\" alt=\"{$itemDestaqueComEspacos}\">
							</div>
							<div class=\"title-produtos\">
								<h2>{$itemDestaqueComEspacos}</h2>
							</div>
						</a>
					</li>";
            }
            ?>

        </ul>
    </div>

</div>
