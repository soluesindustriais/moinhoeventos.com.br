<?php
/**
 * Função para construir a trilha de navegação (breadcrumb) de forma recursiva.
 * 
 * @param array $menuItems Array de itens de menu que pode conter submenus.
 * @param string $urlPagina URL da página atual para verificar o item ativo.
 * @param array $breadcrumb Array de referência para armazenar a trilha construída.
 * @return bool Retorna true se a página atual está no menu, caso contrário, false.
 */
function getBreadcrumb($menuItems, $urlPagina, &$breadcrumb = []) {
    foreach ($menuItems as $item => $data) {
        if ($data['url'] === $urlPagina) {
            array_unshift($breadcrumb, [$item, $data['url']]);
            return true;
        } elseif (!empty($data['submenu']) && is_array($data['submenu'])) {
            if (getBreadcrumb($data['submenu'], $urlPagina, $breadcrumb)) {
                array_unshift($breadcrumb, [$item, $data['url']]);
                return true;
            }
        }
    }
    return false;
}

// Preparando a estrutura do breadcrumb
$breadcrumb = [];
getBreadcrumb($menuItems, $urlPagina, $breadcrumb);
$breadActive = end(array_keys($breadcrumb));

foreach($breadcrumb as $key => $item) {
    addBreadJson($item[1], $item[0]);
}
$breadJsonEncoded = json_encode($breadJsonSchema, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
?>

<div class="breadcrumb" id="breadcrumb">
    <div class="bread__row wrapper-produtos">
        <p class="bread__minisite"><?php echo $cliente_minisite?></p>
        <h1 class="bread__title"><?= htmlspecialchars($h1) ?></h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="<?= htmlspecialchars($link_minisite_subdominio) ?>" itemprop="item" title="Início">
                        <span itemprop="name"><i class="fa-solid fa-house"></i> Início</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>

                <?php
                $position = 2; // Posição inicial para os itens adicionais
                foreach($breadcrumb as $key => $item):
                    if($key === $breadActive): ?>
                        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name"><?= htmlspecialchars($item[0]) ?></span>
                            <meta itemprop="position" content="<?= $position ?>">
                        </li>
                    <?php else: ?>
                        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="<?= htmlspecialchars($link_minisite_subdominio.$item[1]) ?>" itemprop="item" title="<?= htmlspecialchars($item[0]) ?>">
                                <span itemprop="name"><?= htmlspecialchars($item[0]) ?></span>
                            </a>
                            <meta itemprop="position" content="<?= $position ?>">
                        </li>
                    <?php endif;
                    $position++;
                endforeach; ?>
            </ol>
        </nav>
    </div>
</div>
