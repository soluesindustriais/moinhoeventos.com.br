<?
$tagsanalytic = ["id da tag", "G-D23WW3S4NC"];
$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
    $url = $http . "://" . $host . "/";
} else {
    $url = $http . "://" . $host . $dir["dirname"] . "/";
}

// Informações site
$nomeSite      = 'Nocelli Maquinas';
$nomeSatelite = "moinhoeventos.com.br";
$slogan        = 'Movendo sua indústria com qualidade e precisão';
$rua        = 'Rua da Coroa';
$bairro        = 'Vila Guilherme';
$cidade        = 'São Paulo';
$UF          = 'SP';
$cep        = 'CEP: 02047-020';
$cliente_email = "vendas1@nocelli.com.br";
$cliente_telefone = "(11) 2905-4081 (11) 99149-2876";
$idForm = "465";

$latitude      = '-22.546052';
$longitude      = '-48.635514';
$explode      = explode("/", $_SERVER['REQUEST_URI']);
$urlPagina       = end($explode);
$urlPagina       = str_replace('.php', '', $urlPagina);
$urlPagina       == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);

// Configuração de linkagem mini-site

$subdominio = str_replace(" ", "", strtolower(trim(remove_acentos($cliente_minisite))));
$link_minisite = "https://$nomeSatelite/produtos/";
$link_minisite_subdominio = "https://$subdominio.$nomeSatelite/";


// // Navegação localhost!
if (strpos($url, "localhost") !== false) {
    $link_minisite_subdominio = $url;
    $link_minisite = $url . "produtos/";
    if (strpos($url, "/produtos/") !== false) {
        $link_minisite = $url;
        $link_minisite_subdominio = str_replace("produtos/", "", $link_minisite_subdominio); 
    } 
}

//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = 'https://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess, '/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include "$prefix_includes" . "inc/gerador-htaccess.php";
$sigMenuPosition = false;
$sigMenuIcons = [];

//Pasta de imagens, Galeria, url Facebook, etc.
$pastaSocialMedia = "$prefix_includes" . "imagens/informacoes/";

// Menus items json
$jsonMenu = file_get_contents("$prefix_includes" . "js/menu-items.json");
$menuItems = json_decode($jsonMenu, true);
$menuKeys = array_keys($menuItems);


//Breadcrumbs
$breadPosition = 1;
$breadJsonSchema = array(
    "@context" => "https://schema.org",
    "@type" => "BreadcrumbList",
    "itemListElement" => array(
        [
            "@type" => "ListItem",
            "position" => $breadPosition,
            "item" => array(
                "@id" => trim($url, '/'),
                "name" => "Home"
            )
        ]
    )
);

if (!function_exists('addBreadJson')) {
    function addBreadJson($urlBread, $title)
    {
        global $breadPosition, $breadJsonSchema, $url;
        $breadPosition++;
        array_push($breadJsonSchema["itemListElement"], ["@type" => "ListItem", "position" => $breadPosition, "item" => array("@id" => $url . $urlBread, "name" => $title)]);
    }
}

$caminho = '<div class="breadcrumb" id="breadcrumb">
    <div class="bread__row wrapper-produtos">
        <p class="bread__minisite">' . $cliente_minisite . '</p>
        <h1>' . $h1 . '</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <span itemprop="name">' . $h1 . '</span>
                    <meta itemprop="position" content="2">
                </li>
            </ol>
        </nav>
    </div>
</div>';
$caminhocateg = '<div class="breadcrumb" id="breadcrumb">
    <div class="bread__row wrapper-produtos">
        <h1>' . $h1 . '</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <a href="' . $url . 'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name"> Produtos</span>
                    </a>
                    <meta itemprop="position" content="2">
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <span itemprop="name">' . $h1 . '</span>
                    <meta itemprop="position" content="3">
                </li>
            </ol>
        </nav>
    </div>
</div>';

$isMobile = preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
