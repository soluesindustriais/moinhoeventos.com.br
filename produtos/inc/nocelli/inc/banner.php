	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<!-- <div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/bg-header.jpg')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>Title</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<a class="btn" href="<?= $url ?>catalogo" title="Catalogo de Produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="Logo da categoria" title="logo do cliente" class="slick-thumb">
						</div>
					</div>
				</div>
			</div> -->
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner.webp')">
				<div class="content-banner">
					<h2>NOCELLI MAQUINAS</h2>
					<p>Soluções completas para o setor industrial.</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="S	obre nós">Saiba mais</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>