
<section class="wrapper card-informativo">
    <h2>Nossos Princípios</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-pen-to-square"></i>
            <h3>Missão</h3>
            <p>Oferecer soluções eficientes e seguras na compra, venda, desmontagem e transporte de máquinas e plantas industriais, garantindo qualidade, transparência e comprometimento com clientes, fornecedores e colaboradores.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-chart-column"></i>
            <h3>Visão</h3>
        <p>Ser referência no mercado industrial, reconhecida pela excelência em serviços, inovação e cumprimento de prazos, sempre atuando com responsabilidade social e ambiental.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-scale-balanced"></i>
        <h3>Valores</h3>
        <p>Atuamos com ética, transparência e compromisso, garantindo soluções seguras e eficientes. Priorizamos a qualidade, inovação e sustentabilidade, cumprindo prazos e assegurando a satisfação de nossos clientes e parceiros.</p>
    </div>
</div>
</section>
