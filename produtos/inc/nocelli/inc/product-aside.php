<aside class="product-aside">
                <div class="product-aside-infos">
                    <h2><?php echo $h1 ?></h2>
                    <p class="nivel-de-procura">Nível de procura: <b>ALTA</b></p>
                    <button class="botao-cotar" onclick="solicitarocarmento()" title="<?php echo $h1 ?>">Solicite um orçamento</button>
                    <div class="product-aside-icons">
                        <div class="product-aside-card">
                            <div class="product-aside-card-icon">
                                <i class="fa-solid fa-earth-americas"></i>
                            </div>
                            <div class="product-aside-card-info">
                                <h3>Região de Atendimento</h3>
                                <p>Nacional</p>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>