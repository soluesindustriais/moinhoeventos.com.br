<?php

/*====================
|___Marcelo Serra____|
|  Data: 28/08/2020  |
====================*/

class Trata{

//Troca hífen por Underline
public function trocaHifenUnderline($string)
{
    return str_replace("-","_", $string);
}

//Deixa o texto da string em maiúsculo
public function capitalizar($string)
{
    return ucwords($string);
}

//Deixa o texto da string em maiúsculo
public function maiuscula($string)
{
    return mb_strtoupper($string);
}

//Deixa o texto da string em minúsculo
public function minuscula($string)
{
    return mb_strtolower($string);
}

//Trata os acentos das strings e retorna a mesma corrigida
public function trataAcentos($string)
{
  $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
  $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');
  $stringCorrigida = str_replace($comAcentos, $semAcentos, $string);
  return $stringCorrigida;
}

//Retira o hífen entre os espaços da string
public function retiraHifen($string)
{
  $string = str_replace("-", " ", $string);
  return $string;
}

//Insere o hífen entre os espaços da string
public function insereHifen($string)
{
  $string = str_replace(" ", "-", $string);
  return $string;
}

};


?>