<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<? include('inc/geral.php'); ?>
	<? include('inc/jquery.php'); ?>
	<style>
		<?php
		include "css/style.css";
		include "css/normalize.css";
		?>
	</style>
	<?php include 'inc/fancy.php'; ?>

	<link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" onload="this.rel='stylesheet'">
	
	<script>
		<?
		if ($isMobile) {
			include 'js/jquery.slicknav.js';
		}
		
		include("js/vendor/modernizr-2.6.2.min.js");

		?>
	</script>
	<!-- MENU  MOBILE -->
	<!-- /MENU  MOBILE -->
	<title><?= $title . " - " . $nomeSite ?></title>
	<base href="<?= $url ?>">
	<meta name="description" content="<?= ucfirst($desc) ?>">
	<meta name="keywords" content="<?= $h1 . ", " . $key ?>">
	<meta name="geo.position" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>">
	<meta name="geo.region" content="<?= $UF ?>-BR">
	<meta name="ICBM" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?= $url . $urlPagina ?>">
	<?php
	if ($author == '') {
		echo '<meta name="author" content="' . $nomeSite . '">';
	} else {
		echo '<link rel="author" href="' . $author . '">';
	}
	?>

	<link rel="shortcut icon" href="<?= $url ?>imagens/img-home/favicon.png">

	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
	<meta property="og:type" content="article">
	<?php
	if (file_exists($url . $pasta . $urlPagina . "-01.jpg")) {
	?>
		<meta property="og:image" content="<?= $url . $pasta . $urlPagina ?>-01.jpg">
	<?php
	}
	?>
	<meta property="og:url" content="<?= $url . $urlPagina ?>">
	<meta property="og:description" content="<?= $desc ?>">
	<meta property="og:site_name" content="<?= $nomeSite ?>">
	<meta property="fb:admins" content="<?= $idFacebook ?>">