<div class="video-flex">
    <div class="grid videos">
        <div class="col-13 embed-box" onclick="show_empty_iframe(0)">
            <img class="thumb-video" src="https://img.youtube.com/vi/yLYhpe02OD8/maxresdefault.jpg" alt="">
            <h2>MISTURADOR DE DETERGENTES LÍQUIDOS</h2><a href="https://www.youtube.com/watch?v=yLYhpe02OD8" target="_blank" rel="nofollow" title="Veja o vídeo sobre <?= $h1 ?> direto no Youtube">Veja o vídeo sobre MISTURADOR DE DETERGENTES LÍQUIDOS direto no Youtube</a>

        </div>
    </div>
    <div class="grid videos">
        <div class="col-13 embed-box" onclick="show_empty_iframe(1)">
            <img class="thumb-video" src="https://img.youtube.com/vi/xblBJ2pvtoE/maxresdefault.jpg" alt="">
            <h2>Misturadores para mistura de sólidos em líquidos</h2><a href="https://www.youtube.com/watch?v=xblBJ2pvtoE" target="_blank" rel="nofollow" title="Veja o vídeo sobre <?= $h1 ?> direto no Youtube">Veja o vídeo sobre Misturadores para mistura de sólidos em líquidos direto no Youtube</a>

        </div>
    </div>
</div>

<div class="lightbox-iframe" id="lightbox-iframe" onclick="close_lightbox()">
    <div class="iframe-field" id="iframe-field">
        <!-- o iframe sera inserido aqui pelo JS -->
    </div>
</div>


<style>
    .no-pointer-ev {
        pointer-events: none;
    }

    .lightbox-iframe {
        background-color: #000000e3;
        width: 100%;
        height: 100%;
        z-index: 99999;
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        display: none;
        justify-content: center;
        align-items: center;
    }

    .iframe-field {
        padding: 20px;
        width: 50%;
    }


    #empty-iframe {
        width: 100%;
        height: 500px;
        border-radius: 6px;
        box-shadow: 0px 15px 12px #000000c4;
    }

    .thumb-video {
        width: 100%;
        cursor: pointer;
        box-shadow: 1px 3px 4px #2c2c2ca8;
    }

    .embed-box::after {
        position: relative !important;
        bottom: 235px;
        left: 0%;
        text-align: center;
        display: flex;
        justify-content: center;
        cursor: pointer;
    }

    .video-flex {
        display: flex;
        padding: 10px;
        justify-content: center;
    }

    .grid.videos {
        margin: 10px;
    }

    .grid-desc {
        font-size: 1.4em;
    }


    @media only screen and (max-width: 600px) {
        .video-flex {
            flex-wrap: wrap;
        }

        .embed-box::after {
            bottom: 125px;
        }

        .iframe-field {
            padding: 20px;
            width: 100vw;
            display: flex;
            align-items: center;
            height: 69vh;
        }

        #empty-iframe {
            width: 100%;
            height: 205px;
            border-radius: 6px;
            box-shadow: 0px 15px 12px #000000c4;
        }
    }
</style>

<script src="js/iframe.js"></script>