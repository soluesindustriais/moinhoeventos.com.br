<div class="logo-top">
  <a href="<?=$url?>" title="Início">
  <img src="imagens/img-home/logo.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>"></a>
</div>
<ul>
  <li><a href="<?=$url?>" title="Página inicial"><i class="fas fa-home"></i> Início</a></li>

  <li class="dropdown"><a href="<?=$url?>informacoes" title="Informações"><i class="fas fa-align-justify"></i> Informações</a>
  <ul class="sub-menu">
    <? include('inc/coluna-lateral-mpi.php');?>
  </ul>
  <ul></ul>

  
  <li class="dropdown"><a href="<?=$url?>produtos-categoria" title="Produtos"><i class="fas fa-align-justify"></i> Produtos</a>
   <ul class="sub-menu">
   <? include('inc/coluna-lateral-produtos.php');?>


   </ul>
  

  
  
</li>
<li><a href="<?=$url?>sobre-nos" title="Sobre nós"><i class="fas fa-user"></i> Sobre nós</a></li>
<li><a href="<?=$url?>blog" title="Blog"><i class="fas fa-user"></i> Blog</a></li>
<!-- <li>
  <div class="google-search">
    <div>
     <script>
  (function() {
    var cx = '006639092025700158856:wqcmti_znz8';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
      <div class="gcse-searchbox-only"></div>
    </div>
  </div>
</li> -->