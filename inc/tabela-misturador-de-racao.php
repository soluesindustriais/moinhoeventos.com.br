<table class="steelBlueCols">
<thead>
<tr>
<th>Modelo/Potência</th>
<th>300</th>
<th>500</th>
<th>1000</th>
</tr>
</thead>
<tbody>
<tr>
<th>Potência necessária motor elétrico (cv)</th>
<th>1,5 à 2</th>
<th>2 à 3</th>
<th>3 à 5</th>
</tr>
<tr>
<th>Potência necessária motores gasolina/diesel (cv)</th>
<th>4,0</th>
<th>6,0</th>
<th>7,5</th>
</tr>
<tr>
<th>Rotação motor (rpm)</th>
<th>1740</th>
<th>1740</th>
<th>1740</th>
</tr>
<tr>
<th>Rotação da rosca sem fim (rpm)</th>
<th>485</th>
<th>485</th>
<th>485</th>
</tr>
<tr>
<th>Peso (kg)</th>
<th>105</th>
<th>135</th>
<th>180</th>
</tr>
<tr>
<th>Produção (kg/15 min)</th>
<th>300</th>
<th>500</th>
<th>1000</th>
</tr>
<tr>
<th>Altura (mm)</th>
<th>2320</th>
<th>2320</th>
<th>2980</th>
</tr>
<tr>
<th>Largura (mm)</th>
<th>1040</th>
<th>1315</th>
<th>1365</th>
</tr>
</tbody>
</table>