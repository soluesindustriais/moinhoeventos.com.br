<?
$h1         = 'Início';
$title      = 'Início';
$desc       = 'Moinho e Ventos | Cote moinhos de diversos modelos com inúmeras empresas e tudo isso em um só lugar, no maior portal de misturadores e moinhos';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>Misturadores e Moinhos</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>MOINHO DE CAFÉ</h2>
        <p>Moinho de café é um aparelho utilizado para moer os grãos de café, normalmente após estes terem sido torrados. O moedor aplica uma força mecânica considerável para rachar os grãos e quebrá-los, deixando o produto em diferentes estados de granulosidade – esses diferentes estados, por sua vez, são utilizados para formas específicas de preparo do café.</p>
        <a href="<?=$url?>moinho-de-cafe" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>MISTURADOR DE RAÇÃO</h2>
        <p>O Misturador de Ração são construído em aço carbono de primeira qualidade com o eixo central rigorosamente balanceado e rolamentos protegidos por retentores industriais, que garantem a todo o conjunto, Misturador de Ração, uma longa vida útil.</p>
        <a href="<?=$url?>misturador-de-racao" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>MISTURADOR INDUSTRIAL</h2>
        <p>Disponíveis em diversos modelos, o misturador industrial são compactos, robustez e são capazes de processar materiais de composições variadas e atendem a condições específicas de trabalho em uma ampla gama de aplicações do setor industrial. Confira!</p>
        <a href="<?=$url?>misturador-industrial" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0" title="slide step"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0" title="slide step"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0" title="slide step"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>Misturador Industrial</h2>
        <div class="div-img">
          <p data-anime="left-0">Os Misturadores são construído com aço carbono com o eixo central rigorosamente balanceado e rolamentos protegidos por retentores industriais de primeira qualidade, são pontos que garantem a funcionalidade e a vida útil do misturador.</p>
        </div>
        <div class="gerador-svg" data-anime="in">
          <img src="imagens/img-home/moinho.jpg" alt="Automação industrial" title="Automação industrial">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>O agitador industrial é um equipamento utilizado para fazer a mistura entre líquidos, estejam ou não com alguma partícula sólida. <br> 
            Existem diferentes tipos de agitadores industriais, são eles:</p>
            <li><i class="fas fa-angle-right"></i> Agitadores laterais</li>
            <li><i class="fas fa-angle-right"></i> Agitadores verticais</li>
            <li><i class="fas fa-angle-right"></i> Agitadores especiais</li>
            <li><i class="fas fa-angle-right"></i> Agitadores de grande porte</li>
          </ul>
          <a href="<?=$url?>misturador-industrial" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-cogs fa-7x"></i>
            <div>
              <p>SERVIÇO DE MANUTENÇÃO</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="far fa-building fa-7x"></i>
            <div>
              <p>COTE COM DIVERSAS EMPRESAS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-dollar-sign fa-7x"></i>
            <div>
              <p>COMPARE PREÇOS</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>misturador-industrial">
              <div class="fig-img">
                <h2>MISTURADOR INDUSTRIAL</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>moinho-de-bolas">
              <div class="fig-img2">
                <h2>MOINHO DE BOLAS</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>moinho-de-milho">
              <div class="fig-img">
                <h2>MOINHO DE MILHO</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/misturador-de-racao-01.jpg" class="lightbox" title="Misturador de ração">
            <img src="<?=$url?>imagens/img-home/thumbs/misturador-de-racao-01.jpg" title="Misturador de ração" alt="Misturador de ração">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/misturador-de-tambor-02.jpg" class="lightbox"  title="Misturador de Tambor">
            <img src="<?=$url?>imagens/img-home/thumbs/misturador-de-tambor-02.jpg" alt="Misturador de Tambor" title="Misturador de Tambor">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/misturador-duplo-cone-02.jpg" class="lightbox" title="Misturador Duplo Cone">
            <img src="<?=$url?>imagens/img-home/thumbs/misturador-duplo-cone-02.jpg" alt="Misturador Duplo Cone" title="Misturador Duplo Cone">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/misturador-horizontal-de-argamassa-02.jpg" class="lightbox" title="Misturador Horizontal de Argamassa">
            <img src="<?=$url?>imagens/img-home/thumbs/misturador-horizontal-de-argamassa-02.jpg" alt="Misturador Horizontal de Argamassa" title="Misturador Horizontal de Argamassa">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/misturador-intensivo-02.jpg" class="lightbox" title="Misturador Intensivo">
            <img src="<?=$url?>imagens/img-home/thumbs/misturador-intensivo-02.jpg" alt="Misturador Intensivo"  title="Misturador Intensivo">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/moinho-de-cafe-02.jpg" class="lightbox" title="Moinho de Café">
            <img src="<?=$url?>imagens/img-home/thumbs/moinho-de-cafe-02.jpg" alt="Moinho de Café" title="Moinho de Café">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/moinho-de-martelo-02.jpg" class="lightbox" title="Moinho de Martelo">
            <img src="<?=$url?>imagens/img-home/thumbs/moinho-de-martelo-02.jpg" alt="Moinho de Martelo" title="Moinho de Martelo">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/moinho-de-martelo-para-calcario-02.jpg" class="lightbox" title="Moinho de Martelo para Calcário">
            <img src="<?=$url?>imagens/img-home/thumbs/moinho-de-martelo-para-calcario-02.jpg" alt="Moinho de Martelo para Calcário" title="Moinho de Martelo para Calcário">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/moinho-de-martelo-preco-02.jpg" class="lightbox" title="Moinho de Martelo Preço">
            <img src="<?=$url?>imagens/img-home/thumbs/moinho-de-martelo-preco-02.jpg" alt="Moinho de Martelo Preço" title="Moinho de Martelo Preço">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/moinho-de-martelos-para-trator-02.jpg" class="lightbox" title="Moinho de Martelos para Trator">
            <img src="<?=$url?>imagens/img-home/thumbs/moinho-de-martelos-para-trator-02.jpg" alt="Moinho de Martelos para Trator" title="Moinho de Martelos para Trator">
            </a>
            </li>
          </ul>
        </div>
      </div>
    </section>
</main>
<? include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>